package com.phonebook.core.model.repository;

import com.phonebook.core.model.entity.Role;
import com.phonebook.core.model.enumconstant.Status;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Set;

@Repository
public interface RoleRepository extends JpaRepository<Role , Long>{

    Role findByTitle(String title);

    List<Role> findAllByStatus(Status status , Pageable pageable);

    Set<Role> findAllByStatusAndIdIn(Status status , Set<Long> roleIdList);

    Role findByIdAndStatus(long roleId , Status status);
}
