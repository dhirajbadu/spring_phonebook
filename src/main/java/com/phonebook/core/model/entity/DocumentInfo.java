package com.phonebook.core.model.entity;

import com.phonebook.core.model.enumconstant.DIR;
import com.phonebook.core.model.enumconstant.Status;

import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "document_info_table")
public class DocumentInfo extends AbstractEntity<Long>{

    private String title;

    private int size;

    private String name;

    private DIR dir;

    private Status status;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public DIR getDir() {
        return dir;
    }

    public void setDir(DIR dir) {
        this.dir = dir;
    }

    public Status getStatus() {
        return status;
    }

    public void setStatus(Status status) {
        this.status = status;
    }
}
