package com.phonebook.core.model.entity;

import com.phonebook.core.model.enumconstant.Status;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name="country")
public class CountryInfo extends AbstractEntity<Long> {

	@Column(unique = true, nullable = false)
	private String name;

	@Column(unique = true, nullable = false)
	private String ISO;

	@Column(nullable = false)
	private Status status;

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getISO() {
		return ISO;
	}

	public void setISO(String ISO) {
		this.ISO = ISO;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}
}
