package com.phonebook.core.api.impl;

import com.phonebook.core.api.iapi.IDocumentInfoApi;
import com.phonebook.core.model.converter.DocumentInfoConverter;
import com.phonebook.core.model.dto.DocumentInfoDTO;
import com.phonebook.core.model.entity.DocumentInfo;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.DocumentInfoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DocumentInfoApi implements IDocumentInfoApi {

    @Autowired
    private DocumentInfoConverter documentInfoConverter;

    @Autowired
    private DocumentInfoRepository documentInfoRepository;

    @Override
    public DocumentInfo saveAndGetEntity(DocumentInfoDTO documentInfoDTO) {
        return documentInfoRepository.save(documentInfoConverter.convertToEntity(documentInfoDTO));
    }

    @Override
    public List<DocumentInfo> saveListAndGetEntity(List<DocumentInfoDTO> documentInfoDTOList) {
        return documentInfoRepository.save(documentInfoConverter.convertToEntityList(documentInfoDTOList));
    }

    @Override
    public DocumentInfoDTO save(DocumentInfoDTO documentInfoDTO) {
        return documentInfoConverter.convertToDto(saveAndGetEntity(documentInfoDTO));
    }

    @Override
    public void save(long documentInfoId) {

        DocumentInfo documentInfo = documentInfoRepository.findByIdAndStatus(documentInfoId , Status.ACTIVE);

        documentInfo.setStatus(Status.DELETED);

        documentInfoRepository.save(documentInfo);
    }
}
