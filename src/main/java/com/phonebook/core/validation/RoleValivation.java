package com.phonebook.core.validation;

import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.entity.Role;
import com.phonebook.core.model.enumconstant.Permission;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.repository.RoleRepository;
import com.phonebook.web.error.RoleError;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.List;
import java.util.Set;

@Service
public class RoleValivation extends GlobalValidation{

    @Autowired
    private RoleRepository roleRepository;

    RoleError error;

    public RoleError onSave(RoleDTO roleDTO , BindingResult result){

        error = new RoleError();

        boolean valid = true;

        if (roleDTO == null){

            error.setError("invalid request");
            error.setValid(false);

            return error;
        }

        error = validate(result);

        if (!error.isValid()){
            return error;
        }

        valid = valid && checkTitle(roleDTO.getTitle());

        valid = valid && checkPermission(roleDTO.getPermissionSet());

        error.setValid(valid);

        return error;
    }

    public RoleError onUpdate(RoleDTO roleDTO , BindingResult result){

        error = new RoleError();

        boolean valid = true;

        if (roleDTO == null){

            error.setError("invalid request");
            error.setValid(false);

            return error;
        }

        error = validate(result);

        if (!error.isValid()){
            return error;
        }

        valid = valid && checkRole(roleDTO.getRoleId() , roleDTO.getVersion());

        valid = valid && checkTitleOnUpdate(roleDTO.getTitle() , roleDTO.getRoleId());

        valid = valid && checkPermission(roleDTO.getPermissionSet());

        error.setValid(valid);

        return error;
    }

    private RoleError validate(BindingResult result){

        if (result.hasErrors()) {

            List<FieldError> errors = result.getFieldErrors();

            for (FieldError errorResult : errors) {

                if (errorResult.getField().equals("roleId")) {
                    error.setError("invalid role");
                } else if (errorResult.getField().equals("title")) {
                    error.setTitle("invalid title");
                } else if (errorResult.getField().equals("permissionSet")) {
                    error.setPermission("invalid permission");
                }
            }

            error.setValid(false);

            return error;
        }

        error.setValid(true);

        return error;

    }


    private boolean checkRole(long roleId , int verion){

        error.setError(checkLong(roleId , 1 , "role" , true));

        if (!("".equals(error.getError()))){
            return false;
        }

        Role role = roleRepository.findByIdAndStatus(roleId , Status.ACTIVE);

        if (role == null){
            error.setError("role not found");

            return false;
        }

        if (role.getVersion() != verion){
            error.setError("another user already update this role you must go back to list");

            return false;
        }

        return true;
    }

    private boolean checkTitle(String title){

        error.setTitle(checkString(title , 3 , 15 , "title" , true));

        if (!("".equals(error.getTitle()))){
            return false;
        }

        if (roleRepository.findByTitle(title.trim()) != null){

            error.setTitle("this title already in use");

            return false;
        }

        return true;

    }

    private boolean checkTitleOnUpdate(String title , long roleId){

        error.setTitle(checkString(title , 3 , 15 , "title" , true));

        if (!("".equals(error.getTitle()))){
            return false;
        }

        Role role = roleRepository.findByTitle(title.trim());

        if (role != null){

            if (role.getId() != roleId) {
                error.setTitle("this title already in use");

                return false;
            }
        }

        return true;

    }

    private boolean checkPermission(Set<Permission> permissions){

        if (permissions == null){

            error.setPermission("permission can not be blank");

            return false;
        }

        if (permissions.isEmpty()){

            error.setPermission("permission can not be blank");

            return false;
        }

        if (permissions.size() > 50){

            error.setPermission("permissions can not be more than 50");

            return false;
        }

        return true;
    }
}
