package com.phonebook.core.util;

import com.phonebook.core.api.impl.OffsetBasedPageRequest;
import com.phonebook.core.model.dto.UserSessionDTO;
import com.phonebook.core.model.entity.UserSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class ConvertUtil {
	@SuppressWarnings("unused")
	private Logger logger = LoggerFactory.getLogger(ConvertUtil.class);


	public static  final Pageable createPageRequest(int page , int size , String properties , Sort.Direction direction) {

		return new PageRequest(page, size, new Sort(direction, properties));
	}

	public static  final Pageable createPageRequest(int page , int size , String properties , String direction) {

		Sort.Direction dir = Sort.Direction.ASC;

		if ("desc".equalsIgnoreCase(direction)){
			dir = Sort.Direction.DESC;
		}

		return createPageRequest(page , size , properties , dir);
	}

	public static final String getValidProperty(String property , String...properties){
		String result = "id";

		if (property == null){
			return result;
		}

		property = property.trim();

		if ("".equals(property) || property.isEmpty()){
			return result;
		}

		int count = 0;
		for (String v : properties){
			if (property.equals(String.valueOf(count))){
				return v;
			}
			count ++;
		}

		return result;
	}

	public static  final Pageable createOffsetPageRequest(Integer max, Integer offset , String direction , String property) {

		if (max == null)
			max = 10;

		if (offset == null)
			offset = 0;

		if (direction == null)
			direction = "desc";

		Sort.Direction dir = Sort.Direction.ASC;

		if ("desc".equalsIgnoreCase(direction)){
			dir = Sort.Direction.DESC;
		}

		return  new OffsetBasedPageRequest(offset , max , dir , property);

	}

	public static BigDecimal convertDoubleToDecimal(double value) {
		return new BigDecimal(value);
	}

	public static double convertDecimalToDouble(BigDecimal value) {
		return value.doubleValue();
	}


	public static String getDate(Date date){
		SimpleDateFormat dateFormatYear = new SimpleDateFormat("MMM dd, yyyy");

		return dateFormatYear.format(new Date());
	}


	public static double formatter(double value){

		DecimalFormat df = new DecimalFormat("###.###");

		return Double.parseDouble(df.format(value));

	}

	public static BigDecimal formatter(BigDecimal value){

		DecimalFormat df = new DecimalFormat("###.##");

		return BigDecimal.valueOf(Double.parseDouble(df.format(value)));

	}

	public static String getOrderNo(int length) {

		String charString = Long.toString(System.currentTimeMillis());
		String[] arrayString = charString.split("");
		StringBuilder randomCode = new StringBuilder();
		for (int i = 0; i < length; i++) {
			int randomIndex = (int) (Math.random() * charString.length());
			randomCode.append(arrayString[randomIndex]);
		}
		/*
		 * long uniqueId = (long) (Math.random() * 10000L); String orderNo =
		 * Long.toString(uniqueId);
		 */

		System.out.println("generated code >>>> " + randomCode);
		return randomCode.toString();
	}


	/*public static String CreateHash(String key, String password, String hash) throws ClientException {
		try {
			byte[] decodedKey = (key).getBytes();
			SecretKeySpec keySpec = new SecretKeySpec(decodedKey, hash);
			Mac mac = Mac.getInstance(hash);
			mac.init(keySpec);
			byte[] dataBytes = password.getBytes("UTF-8");
			byte[] signatureBytes = mac.doFinal(dataBytes);
			String encoded = new String(Base64.encodeBase64(signatureBytes), "UTF-8");
			System.out.println("Prepared Encoded Signature :" + encoded);
			return encoded;
		} catch (Exception e) {
			throw new ClientException("Service Down !!!");
		}
	}
*/
	public static List<UserSessionDTO> convertSessionList(List<UserSession> userSession) {
		List<UserSessionDTO> dtoList = new ArrayList<UserSessionDTO>();
		for (UserSession session : userSession) {
			dtoList.add(convertSession(session));
		}
		return dtoList;
	}

	public static UserSessionDTO convertSession(UserSession session) {

		UserSessionDTO dto = new UserSessionDTO();
		dto.setMobileNo(session.getUser().getUsername());
		dto.setId(session.getId());
		dto.setSessionId(session.getSessionId());
		dto.setUserId(session.getUser().getId());

		return dto;
	}
}