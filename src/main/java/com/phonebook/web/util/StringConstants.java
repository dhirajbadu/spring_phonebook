package com.phonebook.web.util;

public class StringConstants {

	public static final String LASTPAGE = "lastpage";
	public static final String CURRENTPAGE = "currentpage";
	public static final String PAGELIST = "pagelist";
	public static final String TERM = "term";

	public static final String ASC = "asc";
	public static final String DESC = "desc";
    public static final String LEDGER_LIST = "ledgerInfoList";

    public static final  String CITY_LIST = "cityList";
	public static final String CITY = "city";
	public static final String CITY_ERROR = "cityError";

	public static final String ERROR = "error";
	public static final String MESSAGE = "message";

	public static final String STATE_LIST = "stateList";
	public static final String STATE = "state";

	public static final String TEMPLATE_LIST = "templateList";
	public static final String TEMPLATE = "template";
	public static final String TEMPLATE_TYPE_LIST = "templateTypeList";

	public static final String COUNTRY_LIST = "countryList";
	public static final String COUNTRY = "country";

	public static final String USER_LIST = "userList";
	public static final String USER = "user";

	public static final String USER_ERROR = "userError";
	public static final String USER_TYPE_LIST = "userTypeList";

	public static final String TOTAL_USER = "totalUser";
	public static final String TOTAL_UNIT = "totalUnit";
	public static final String THIS_YEAR_TOTAL_UNIT = "thisYearUnit";
	public static final String TOTAL_AMOUNT = "totalAmount";
	public static final String CART_SALE_DATA = "cartSaleDate";

	public static final String ROLE_LIST = "roleList";
	public static final String ROLE = "role";
	public static final String ROLE_ERROR = "roleError";

	public static final String PERMESSION_LIST = "permissionList";
	public static final String CHARGETYPE_LIST = "chargeTypeList";

	public static final String BRANCH_LIST = "branchList";
	public static final String BRANCH = "branch";
	public static final String BRANCH_ERROR = "branchError";

	public static final String WARDINFO_LIST = "wardList";
	public static final String WARDINFO = "ward";
	public static final String WARDINFO_ERROR = "wardError";

	public static final String INV_CHRG_TEMPLATE_LIST = "invChargeTemplateList";
	public static final String INV_CHRG_TEMPLATE = "invChargeTemplate";
	public static final String INV_CHRG_TEMPLATE_ERROR = "invChargeTemplateError";

	public static final String BLOCKINFO_LIST = "blockList";
	public static final String BLOCKINFO = "block";
	public static final String BLOCKINFO_ERROR = "blockError";

	public static final String FISCAL_YEAR_LIST = "fiscalYearList";
	public static final String FISCAL_YEAR = "fiscalYear";

	public static final String RATETYPE_LIST = "rateTypeList";
	public static final String RATE = "rateInfo";
	public static final String RATE_ERROR = "rateInfoError";

	public static final String CLIENTTYPE_LIST = "clientTypeList";
	public static final String CLIENTTYPE = "clientType";
	public static final String CLIENTTYPEERROR = "clientTypeError";

	public static final String CLIENTINFO_LIST = "clientInfoList";
	public static final String CLIENTINFO = "clientInfo";
	public static final String CLIENTUNITINFO = "clientUnitInfo";
	public static final String CLIENTUNITINFO_ERROR = "clientUnitInfoError";
	public static final String CLIENTPROFILEINFO = "clientProfileInfo";
	public static final String CLIENT_PROFILE_ERROR = "clientProfileInfoError";


	public static final String AMOUNT = "amount";
	public static final String CODENUMBER = "codeNumber";

	public static final String TOTAL_UNIT_OF_MONTH = "totalunitofmonth";
	public static final String TOTAL_UNIT_OF_MONTH_LIST = "totalunitofmonthlist";

	public static final String TOTAL_UNIT_OF_YEAR = "totalUnitOfYear";
	public static final String TOTAL_UNIT_OF_YEAR_LIST = "totalUnitOfYearList";

	public static final String TOTAL_UNIT_OF_YEAR_OF_CLIENT = "totalUnitOfYearOfClient";
	public static final String TOTAL_UNIT_OF_YEAR_OF_CLIENT_LIST = "totalUnitOfYearOfClientList";

	public static final String ACCOUNT_INFO = "accountInfo";
	public static final String INVOICE_INFO = "invoiceInfo";
	public static final String INVOICE_ERROR = "invoiceInfoError";
	public static final String ACCOUNT_INFO_LIST = "accountInfoList";
	public static final String ACCOUNT_TYPE = "accountType";
}