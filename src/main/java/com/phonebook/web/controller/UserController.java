package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.IRoleApi;
import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.core.api.iapi.IUserApi;
import com.phonebook.core.model.dto.GeneralMessage;
import com.phonebook.core.model.dto.UserDTO;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.enumconstant.UserType;
import com.phonebook.core.validation.UserValidation;
import com.phonebook.web.error.UserError;
import com.phonebook.web.util.AuthenticationUtil;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.PageInfo;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@Controller
@RequestMapping("user")
public class UserController {

    @Autowired
    private IUserApi userApi;

    @Autowired
    private UserValidation userValidation;

    @Autowired
    private IRoleApi roleApi;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private ISendMailSSL sendMailSSL;

    @PreAuthorize("hasRole('Role_User_View')")
    @GetMapping("/list")
    public String list(
            @RequestParam(value = "pageNo", required = false) Integer page,
            ModelMap modelMap) {

        try {

            if (page == null) {
                page = 1;
            }

            if (page < 1) {
                page = 1;
            }

            int currentpage = page - 1;

            long totalList = userApi.countList();

            int totalpage = (int) Math.ceil(totalList / PageInfo.pageList);

            if (currentpage > totalpage || currentpage < 0) {
                currentpage = 0;
            }

            List<Integer> pagesnumbers = PageInfo.PageLimitCalculator(page,
                    totalpage, PageInfo.numberOfPage);

            modelMap.put(StringConstants.USER_LIST,
                    userApi.listWithRole(currentpage, (int) PageInfo.pageList));
            modelMap.put(StringConstants.LASTPAGE, totalpage);
            modelMap.put(StringConstants.CURRENTPAGE, page);
            modelMap.put(StringConstants.PAGELIST, pagesnumbers);

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
            throw e;
        }

        return "user/list";

    }

    @PreAuthorize("hasRole('Role_User_View')")
    @GetMapping("/show")
    public String show(@RequestParam("userId") long userId, ModelMap modelMap,
                       RedirectAttributes redirectAttributes) {

        try {

            UserDTO userDTO = userApi.show(userId);

            if (userDTO == null) {

                redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                        "user not found");

                return "redirect:/user/list";
            }

            modelMap.put(StringConstants.USER, userDTO);

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
            throw e;
        }

        return "user/show";

    }

    @GetMapping("/activate")
    public String activate(@RequestParam("token") String token, RedirectAttributes redirectAttributes) {

        try {

            boolean valid = userValidation.validateActivation(token);
            if (valid) {
                userApi.activateUser(token);
                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "Validation Successfull. Please Login");
                return "redirect:/";
            } else {
                redirectAttributes
                        .addFlashAttribute(StringConstants.ERROR, "User already Valid Or Token doesn't match. Please contact Operator");
                return "redirect:/";
            }

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
            throw e;
        }

    }

    @PreAuthorize("hasRole('Role_User_Create')")
    @GetMapping("/add")
    public String add(ModelMap modelMap) {

        modelMap.put(StringConstants.ROLE_LIST, roleApi.list(Status.ACTIVE));
        modelMap.put(StringConstants.USER, new UserDTO());
        modelMap.put(StringConstants.USER_TYPE_LIST, UserType.values());

        return "user/add";
    }

    @PreAuthorize("hasAnyRole('System' , 'Admin' , 'Data_entry')")
    @GetMapping("/changepassword")
    public String changePassword() {

        return "user/changepassword";
    }

    @PreAuthorize("hasAnyRole('System' , 'Admin' , 'Data_entry')")
    @PostMapping("/updatepassword")
    public String updatePassword(RedirectAttributes redirectAttributes,
                                 @RequestParam("oldpassword") String oldpassword,
                                 @RequestParam("newpassword") String newpassword,
                                 @RequestParam("repassword") String repassword,
                                 HttpServletRequest request, HttpServletResponse response)
            throws IOException {

        if (oldpassword == null || newpassword == null || repassword == null) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                    "please fill the full form");
            return "redirect:/user/changepassword";
        }

        if ("".equals(oldpassword.trim()) || "".equals(newpassword.trim())
                || "".equals(repassword.trim())) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                    "please fill the full form");
            return "redirect:/user/changepassword";
        }

        UserDTO currentUser = AuthenticationUtil.getCurrentUser();

        if (!passwordEncoder.matches(oldpassword, currentUser.getPassword())) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                    "invalid current password");
            return "redirect:/user/changepassword";
        }

        if (!newpassword.equals(repassword)) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                    "new password did not matched with confirm password");
            return "redirect:/user/changepassword";
        }

        if (newpassword.trim().length() < 5) {
            redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                    "new password must be greater than 5 length");
            return "redirect:/user/changepassword";
        }

        try {
            userApi.changePassword(currentUser.getUserId(), newpassword,
                    request, response);
            redirectAttributes.addFlashAttribute(StringConstants.MESSAGE,
                    "password changed successfully");

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "redirect:/login";
    }

    @PostMapping("/forgetpass")
    public String forgetPassword(RedirectAttributes redirectAttributes, @RequestParam("username") String username, @RequestParam(name = "g-recaptcha-response") String recaptchaResponse, HttpServletRequest request, HttpServletResponse response){

        try {

            synchronized (this.getClass()) {
                GeneralMessage gm = userValidation.resetPass(username, recaptchaResponse, request.getRemoteAddr());
                if (!gm.isValid()) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR, gm.getMessage());
                } else {
                    redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "Password reset message sent to your email. Please check email");
                    userApi.resetPasswordComposeCode(username, request);
                }

            }
        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
        }


        return "redirect:/";
    }

    @GetMapping("/resetpass")
    public String resetPass(@RequestParam("token") String token,
                            RedirectAttributes redirectAttributes, ModelMap map) {

        try {
            map.put("vcode", token);

            GeneralMessage gm = userValidation.resetPassFinal(token);
            if (gm.isValid()) {
                return "dashboard/resetPass";
            } else {
                redirectAttributes
                        .addFlashAttribute(StringConstants.ERROR,
                                gm.getMessage());
                return "redirect:/";
            }

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
            throw e;
        }

    }

    @PostMapping("/resetpass")
    public String resetPass(@RequestParam("token") String token, String newPass, String reNewPass,
                            RedirectAttributes redirectAttributes) {

        try {

            GeneralMessage gm = userValidation.validateResetPass(token, newPass, reNewPass);
            if (gm.isValid()) {

                userApi.changePasswordReset(token, newPass);
                redirectAttributes
                        .addFlashAttribute(StringConstants.MESSAGE,
                                "Password Changed Successfully");
                return "redirect:/";
            } else {
                redirectAttributes
                        .addFlashAttribute(StringConstants.ERROR,
                                gm.getMessage());

                return "redirect:/user/resetpass?token="+token;
            }

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);
            throw e;
        }

    }

    @PreAuthorize("hasRole('Role_User_Create')")
    @PostMapping("/save")
    public String save(@ModelAttribute("user") UserDTO userDTO,
                       BindingResult result, ModelMap modelMap,
                       RedirectAttributes redirectAttributes, HttpServletRequest request)
            throws IOException {

        try {

            synchronized (this.getClass()) {

                UserError error = userValidation.onSave(userDTO, result);

                if (!error.isValid()) {

                    modelMap.put(StringConstants.ROLE_LIST,
                            roleApi.list(Status.ACTIVE));
                    modelMap.put(StringConstants.USER, userDTO);
                    modelMap.put(StringConstants.USER_TYPE_LIST,
                            UserType.values());
                    modelMap.put(StringConstants.USER_ERROR, error);

                    return "user/add";
                }

                userDTO = userApi.save(userDTO, request);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE,
                        "user created successfully");
            }

        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "redirect:/user/show?userId=" + userDTO.getUserId();
    }

    @PreAuthorize("hasRole('Role_User_Update')")
    @GetMapping("/edit")
    public String edit(@RequestParam("userId") long userId, ModelMap modelMap,
                       RedirectAttributes redirectAttributes) {

        try {

            UserDTO userDTO = userApi.show(userId);

            if (userDTO == null) {
                redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                        "user not found");

                return "redirect:/user/list";
            }

            modelMap.put(StringConstants.ROLE_LIST, roleApi.list(Status.ACTIVE));
            modelMap.put(StringConstants.USER, userDTO);
            modelMap.put(StringConstants.USER_TYPE_LIST, UserType.values());

        } catch (Exception e) {
            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "user/edit";
    }

    @PreAuthorize("hasRole('Role_User_Update')")
    @PostMapping("/update")
    public String update(@ModelAttribute("user") UserDTO userDTO,
                         BindingResult result, ModelMap modelMap,
                         RedirectAttributes redirectAttributes) throws IOException {

        try {

            synchronized (this.getClass()) {

                UserError error = userValidation.onUpdate(userDTO, result);

                if (!error.isValid()) {

                    modelMap.put(StringConstants.ROLE_LIST,
                            roleApi.list(Status.ACTIVE));
                    modelMap.put(StringConstants.USER, userDTO);
                    modelMap.put(StringConstants.USER_TYPE_LIST,
                            UserType.values());
                    modelMap.put(StringConstants.USER_ERROR, error);

                    return "user/edit";
                }

                userApi.update(userDTO);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE,
                        "user updated successfully");
            }

        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "redirect:/user/show?userId=" + userDTO.getUserId();
    }

    @PreAuthorize("hasRole('Role_User_Update')")
    @GetMapping("/enable")
    public String enable(@RequestParam("userId") long userId,
                         @RequestParam("enable") boolean enable, ModelMap modelMap,
                         RedirectAttributes redirectAttributes) {

        try {

            synchronized (this.getClass()) {
                UserDTO userDTO = userApi.getById(userId);

                if (userDTO == null) {

                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user not found");

                    return "redirect:/user/list";
                }

                if (enable && userDTO.isEnabled()) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user already activated");

                    return "redirect:/user/show?userId=" + userId;
                }

                if (!enable && !userDTO.isEnabled()) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user already inactivated");

                    return "redirect:/user/show?userId=" + userId;
                }

                userApi.enable(userId, enable);

                if (enable) {
                    redirectAttributes.addFlashAttribute(
                            StringConstants.MESSAGE,
                            "user activated successfully");
                } else {
                    redirectAttributes.addFlashAttribute(
                            StringConstants.MESSAGE,
                            "user inactivated successfully");
                }
            }
        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "redirect:/user/show?userId=" + userId;
    }

    @PreAuthorize("hasRole('Role_User_Update')")
    @GetMapping("/lock")
    public String lock(@RequestParam("userId") long userId,
                       @RequestParam("lock") boolean lock, ModelMap modelMap,
                       RedirectAttributes redirectAttributes) {

        try {

            synchronized (this.getClass()) {

                UserDTO userDTO = userApi.getById(userId);

                if (userDTO == null) {

                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user not found");

                    return "redirect:/user/list";
                }

                if (lock && userDTO.getStatus().equals(Status.ACTIVE)) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user already unlocked");

                    return "redirect:/user/show?userId=" + userId;
                }

                if (!lock && userDTO.getStatus().equals(Status.INACTIVE)) {
                    redirectAttributes.addFlashAttribute(StringConstants.ERROR,
                            "user already locked");

                    return "redirect:/user/show?userId=" + userId;
                }

                userApi.lock(userId, lock);

                if (lock) {
                    redirectAttributes
                            .addFlashAttribute(StringConstants.MESSAGE,
                                    "user locked successfully");
                } else {
                    redirectAttributes.addFlashAttribute(
                            StringConstants.MESSAGE,
                            "user unlocked successfully");
                }
            }
        } catch (Exception e) {

            LoggerUtil.logException(this.getClass(), e, sendMailSSL);

            throw e;
        }

        return "redirect:/user/show?userId=" + userId;
    }

}
