package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.core.api.iapi.IUserApi;
import com.phonebook.web.util.AuthenticationUtil;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Controller
public class HomeController {

	@Autowired
	private IUserApi userApi;


	@Autowired
	private ISendMailSSL sendMailSSL;

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String index(@RequestParam(value = "message" , required = false) String message, @RequestParam(value = "error" , required = false) String error, HttpServletRequest request, RedirectAttributes redirectAttributes) throws IOException {

		if (AuthenticationUtil.getCurrentUser() != null) {
			return "redirect:/dashboard";
		} else {
			return "dashboard/login";
		}
	}

	@PreAuthorize("hasAnyRole('System' , 'Admin' , 'Data_entry')")
	@RequestMapping(value = "/dashboard", method = RequestMethod.GET)
	public String dashboard(ModelMap modelMap) {

		modelMap.put(StringConstants.TOTAL_USER , userApi.countList());

		return "dashboard/index";

	}

	@RequestMapping(value = "login", method = RequestMethod.GET)
	public String getLogin() {

		if (AuthenticationUtil.getCurrentUser() == null) {
			return "dashboard/login";
		}

		return "redirect:/dashboard";

	}

	@RequestMapping(value = "logout", method = RequestMethod.GET)
	public String logout( HttpServletRequest request, HttpServletResponse response) {

		try {

			Authentication auth = SecurityContextHolder.getContext().getAuthentication();

			if (auth != null) {
				new SecurityContextLogoutHandler().logout(request, response, auth);
				return "redirect:/login";
			}
		}catch (Exception e){
			LoggerUtil.logException(this.getClass() , e , sendMailSSL);
		}


		return "redirect:/";

	}

	@RequestMapping(value = "/login/{error}", method = RequestMethod.GET)
	public String getLoginWithError(@PathVariable(value = "error") String error, HttpServletRequest request, ModelMap modelMap) throws IOException {

		if (AuthenticationUtil.getCurrentUser() == null) {

			error = error.replace("_" , " ");

			modelMap.put(StringConstants.ERROR, error);

			return "dashboard/login";
		}

		return "redirect:/dashboard";

	}


	@RequestMapping(value = "/404", method = RequestMethod.GET)
	public String error(HttpServletRequest request) {

		return "static/404";
	}
	
	@RequestMapping(value = "/500", method = RequestMethod.GET)
	public String eroor(HttpServletRequest request) {

		return "static/500";
	}

	@GetMapping("/403")
	public String accessDeniedPage(ModelMap model) {

		return "static/403";
	}

}
