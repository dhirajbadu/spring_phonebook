package com.phonebook.web.controller;


import com.phonebook.core.api.iapi.ICityInfoApi;
import org.springframework.beans.factory.annotation.Autowired;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/city")
public class CityController {

	@Autowired
	private ICityInfoApi cityInfoApi;

	@PreAuthorize("hasRole('Role_City_View')")
	@GetMapping("/list")
	public String list(ModelMap modelMap){

		//modelMap.put(StringConstants.CITY_LIST , cityInfoApi.list());

		return "city/list";
	}
}

	

