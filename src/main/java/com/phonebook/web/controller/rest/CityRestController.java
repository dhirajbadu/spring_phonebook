package com.phonebook.web.controller.rest;

import com.phonebook.core.api.iapi.ICityInfoApi;
import com.phonebook.core.model.dto.CityInfoDTO;
import com.phonebook.core.model.enumconstant.Status;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/ajax/city")
public class CityRestController {

    @Autowired
    private ICityInfoApi cityInfoApi;

    @GetMapping("/list")
    @PreAuthorize("hasRole('Role_City_View')")
    public Map<String , Object> ajaxList(@RequestParam("length") Integer max ,
                                         @RequestParam("start") Integer offset ,
                                         @RequestParam("order[0][column]") String sort,
                                         @RequestParam("order[0][dir]") String order,
                                         HttpServletRequest request ,
                                         HttpServletResponse response) {
        Map<String , Object> resultMap = new HashMap<>();

        if (max == null)
            max = 10;

        if (offset == null)
            offset = 0;

        if (sort ==null)
            sort = "0";

        if (order == null)
            order = "asc";

        Map<String , String> orderMapper = get_pageable();

        sort = orderMapper.getOrDefault(sort, "id");

        List<CityInfoDTO> cityInfoDTOList = cityInfoApi.list(max , offset , order , sort);
        List<String[]> arrStr = new ArrayList<>();

        for (CityInfoDTO cityInfoDTO : cityInfoDTOList) {
            String[] arr = new String[3];

            arr[0] = cityInfoDTO.getCityId().toString();
            arr[1] = cityInfoDTO.getCityName();
            arr[2] = cityInfoDTO.getStateName();

            arrStr.add(arr);

        }

        resultMap.put("data" , arrStr);
        resultMap.put("recordsTotal" , cityInfoApi.cityCount(Status.ACTIVE));
        resultMap.put("recordsFiltered" , cityInfoApi.cityCount(Status.ACTIVE));

        return resultMap;
    }

    private Map<String , String> get_pageable(){

        Map<String , String> orderMapper = new HashMap<>();
        orderMapper.put("0" , "id");
        orderMapper.put("1" , "name");

        return orderMapper;
    }
}
