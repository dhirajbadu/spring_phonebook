package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.IRoleApi;
import com.phonebook.core.api.iapi.ISendMailSSL;
import com.phonebook.core.model.dto.RoleDTO;
import com.phonebook.core.model.enumconstant.Permission;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.validation.RoleValivation;
import com.phonebook.web.error.RoleError;
import com.phonebook.web.util.LoggerUtil;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
@RequestMapping("role")
public class RoleController {

    @Autowired
    private IRoleApi roleApi;

    @Autowired
    private RoleValivation roleValivation;

    @Autowired
    private ISendMailSSL sendMailSSL;

    @PreAuthorize("hasRole('Role_Role_View')")
    @GetMapping("/list")
    public String list(ModelMap modelMap){

        modelMap.put(StringConstants.ROLE_LIST , roleApi.list(Status.ACTIVE));

        return "role/list";
    }

    @PreAuthorize("hasRole('Role_Role_Create')")
    @GetMapping("/add")
    public String add(ModelMap modelMap){

        modelMap.put(StringConstants.PERMESSION_LIST , Permission.values());
        modelMap.put(StringConstants.ROLE , new RoleDTO());

        return "role/add";
    }

    @PreAuthorize("hasRole('Role_Role_Create')")
    @PostMapping("/save")
    public String save(@ModelAttribute("role")RoleDTO roleDTO , BindingResult result , ModelMap modelMap , RedirectAttributes redirectAttributes){

        try {

            synchronized (this.getClass()) {

                RoleError error = roleValivation.onSave(roleDTO , result);

                if (!error.isValid()){

                    modelMap.put(StringConstants.PERMESSION_LIST , Permission.values());
                    modelMap.put(StringConstants.ROLE , roleDTO);
                    modelMap.put(StringConstants.ROLE_ERROR , error);

                    return "role/add";
                }

                roleApi.save(roleDTO);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "role created successfully");
            }
        }catch (Exception e){
            LoggerUtil.logException(this.getClass() , e , sendMailSSL);
            throw e;
        }

        return "redirect:/role/list";
    }

    @PreAuthorize("hasRole('Role_Role_Update')")
    @GetMapping("/edit")
    public String edit(@RequestParam("roleId")long roleId , ModelMap modelMap , RedirectAttributes redirectAttributes){

        try {

            RoleDTO roleDTO = roleApi.show(roleId , Status.ACTIVE);

            if (roleDTO == null){

                redirectAttributes.addFlashAttribute(StringConstants.ERROR, "role not found");

                return "redirect:/role/list";
            }

            modelMap.put(StringConstants.PERMESSION_LIST , Permission.values());
            modelMap.put(StringConstants.ROLE , roleDTO);

        }catch (Exception e){
            LoggerUtil.logException(this.getClass() , e , sendMailSSL);
            throw e;
        }

        return "role/edit";
    }

    @PreAuthorize("hasRole('Role_Role_Update')")
    @PostMapping("/update")
    public String update(@ModelAttribute("role")RoleDTO roleDTO , BindingResult result , ModelMap modelMap , RedirectAttributes redirectAttributes){

        try {

            synchronized (this.getClass()) {

                RoleError error = roleValivation.onUpdate(roleDTO , result);

                if (!error.isValid()){

                    modelMap.put(StringConstants.PERMESSION_LIST , Permission.values());
                    modelMap.put(StringConstants.ROLE , roleDTO);
                    modelMap.put(StringConstants.ROLE_ERROR , error);

                    return "role/edit";
                }

                roleApi.update(roleDTO);

                redirectAttributes.addFlashAttribute(StringConstants.MESSAGE, "role updated successfully");
            }

        }catch (Exception e){
            LoggerUtil.logException(this.getClass() , e , sendMailSSL);
            throw e;
        }

        return "redirect:/role/list";
    }

}
