package com.phonebook.web.controller;

import com.phonebook.core.api.iapi.ICountryInfoApi;
import com.phonebook.web.util.StringConstants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/country")
public class CountryController {
	
    @Autowired
    private ICountryInfoApi countryService;

	@PreAuthorize("hasRole('Role_Country_View')")
    @GetMapping("/list")
    public String list(ModelMap modelMap){

		modelMap.put(StringConstants.COUNTRY_LIST , countryService.list());

		return "country/list";
    }
}
