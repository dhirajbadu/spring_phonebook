package com.phonebook.web.startup;

import com.phonebook.core.model.entity.*;
import com.phonebook.core.model.enumconstant.Permission;
import com.phonebook.core.model.enumconstant.Status;
import com.phonebook.core.model.enumconstant.UserType;
import com.phonebook.core.model.repository.*;
import com.phonebook.core.util.MacAddressUtls;
import com.phonebook.web.util.LoggerUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.annotation.PostConstruct;
import java.util.*;


public class Startup {

	@Autowired
	private PasswordEncoder passwordEncoder;

	@Autowired
	private UserRepository userRepository;

	@Autowired
	private RoleRepository roleRepository;

	@Autowired
	private CountryInfoRepository countryInfoRepository;

	@Autowired
	private StateInfoRepository stateInfoRepository;

	@Autowired
	private CityInfoRepository cityInfoRepository;


	@PostConstruct
	public void initialize() throws Exception {

		macValidation();
		Role role = createRole("Role_System");

		if (role != null) {

			Set<Role> roleSet = new HashSet<>();

			roleSet.add(role);

			createUser("system", "1234567", Status.ACTIVE , roleSet);
		}

		CountryStarter();

		setUpRoles();
	}

	private void macValidation() throws Exception {
		//fa:16:3e:23:0f:3f
		//54:e1:ad:53:17:06

		String[] deviceMacAddr = new String[]{"54:e1:ad:53:17:06"};
		String[] resultMacAddr = new String[]{MacAddressUtls.getMacAddress()};

		if (!MacAddressUtls.isValidMacAddress( deviceMacAddr, resultMacAddr)){
			LoggerUtil.logException(this.getClass() , new Exception("invalid server"));
			System.exit(0);
			throw new Exception("invalid server");
		}

	}

	private void setUpRoles(){

		setUpRole("Role_User_Role_R" , Permission.USER_VIEW , Permission.ROLE_VIEW);
		setUpRole("Role_User_Role_W" , Permission.USER_CREATE , Permission.USER_UPDATE , Permission.USER_DELETE , Permission.ROLE_CREATE , Permission.ROLE_UPDATE , Permission.ROLE_DELETE);

	}


	private void setUpRole(String title , Permission... permissions){

		if (roleRepository.findByTitle(title) == null){

			Role role = new Role();

			role.setStatus(Status.ACTIVE);
			role.setTitle(title);
			role.setPermissionSet(new HashSet<>(Arrays.asList(permissions)));

			roleRepository.save(role);
		}
	}


	private Role createRole(String title){

		Role role = roleRepository.findByTitle(title);
		if (role == null){


			Set<Permission> permissionSet = new HashSet<>();

			permissionSet.addAll(Arrays.asList(Permission.values()));

			role = new Role();

			role.setStatus(Status.ACTIVE);
			role.setTitle(title);
			role.setPermissionSet(permissionSet);

			return roleRepository.save(role);

		}else if (role.getPermissionSet().size() != Arrays.asList(Permission.values()).size()){
			Set<Permission> permissionSet = new HashSet<>();

			permissionSet.addAll(Arrays.asList(Permission.values()));

			role.setPermissionSet(permissionSet);

			return roleRepository.save(role);
		}

		return null;
	}

	private void createUser(String userName,  String password, Status status , Set<Role> roleSet) {
		try {


			if (userRepository.findByUsername(userName) == null) {

				User dto = new User();

				dto.setUsername(userName);
				dto.setPassword(passwordEncoder.encode(password));
				dto.setStatus(status);
				dto.setEnabled(true);
				dto.setRoleSet(roleSet);
				dto.setUserType(UserType.SYSTEM);

				userRepository.save(dto);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	private CountryInfo createCountry(String name, String ISO,  Status status) {

			if (countryInfoRepository.findByName(name) == null) {

				CountryInfo dto = new CountryInfo();

				dto.setName(name);
				dto.setISO(ISO);
				dto.setStatus(status);

				return countryInfoRepository.save(dto);
			}

			return null;
	}


	public void CountryStarter() {

		CountryInfo c = createCountry("Nepal" , "NPL" , Status.ACTIVE);

		if (c != null) {

			HashMap<CountryInfo, String> statesmap = new HashMap<CountryInfo, String>();

			statesmap.put(c, "Mechi,Koshi,Sagarmatha,Janakpur,Bagmati,Narayani,Gandaki,Lumbini,Dhaulagiri,Rapti,Karnali,Bheri,Seti,Mahakali");

			List<StateInfo> s = createState(statesmap);

			List<String> cityMap = new ArrayList<String>();

			cityMap.add("Ilam,Jhapa,Panchthar,Taplejung");
			cityMap.add("Bhojpur,Dhankuta,Morang,Sankhuwasabha,Sunsari,Terhathum");
			cityMap.add("Khotang,Okhaldhunga,Saptari,Siraha,Solukhumbu,Udayapur");
			cityMap.add("Dhanusa,Dholkha,Mahottari,Ramechhap,Sarlahi,Sindhuli");
			cityMap.add("Bhaktapur,Dhading,Kathmandu,Kavrepalanchok,Lalitpur,Nuwakot,Rasuwa,Sindhupalchok ");
			cityMap.add("Bara,Chitwan,Makwanpur,Parsa,Rautahat");
			cityMap.add("Gorkha,Kaski,Lamjung,Manang,Syangja,Tanahu");
			cityMap.add("Arghakhanchi,Gulmi,Kapilvastu,Nawalparasi,Palpa,Rupandehi");
			cityMap.add("Baglung,Mustang,Myagdi,Parbat");
			cityMap.add("Dang,Pyuthan,Rolpa,Rukum,Salyan");
			cityMap.add("Dolpa,Humla,Jumla,Kalikot,Mugu");
			cityMap.add("Banke,Bardiya,Dailekh,Jajarkot,Surkhet");
			cityMap.add("Achham,Bajhang,Bajura,Doti,Kailali");
			cityMap.add("Baitadi,Dadeldhura,Darchula,Kanchanpur");

			HashMap<StateInfo, String> citymap = new HashMap<StateInfo, String>();

			int start = 0;

			for (StateInfo ss : s) {

				citymap.put(ss, cityMap.get(start));
				start++;

			}

			createCity(citymap);
		}

	}

	private List<StateInfo> createState(HashMap<CountryInfo, String> statesmap) {

		List<StateInfo> st = new ArrayList<StateInfo>();

		for (Map.Entry<CountryInfo, String> entry : statesmap.entrySet()) {

			CountryInfo c = entry.getKey();

			String[] values = entry.getValue().split(",");

			for (int i = 0; i < values.length; i++) {

				StateInfo s = stateInfoRepository.findByName(values[i]);

				if (s == null) {
					s = new StateInfo();
					s.setCountryInfo(c);
					s.setName(values[i]);
					s.setStatus(Status.ACTIVE);
					s = stateInfoRepository.save(s);
					st.add(s);
				}

			}
		}
		return st;
	}

	private void createCity(HashMap<StateInfo, String> citymap) {
		for (Map.Entry<StateInfo, String> entry : citymap.entrySet()) {

			StateInfo s = entry.getKey();
			String[] values = entry.getValue().split(",");
			for (int i = 0; i < values.length; i++) {

				CityInfo c = cityInfoRepository.findByName(values[i]);
				if (c == null) {
					c = new CityInfo();
					c.setStateInfo(s);
					c.setName(values[i]);
					c.setStatus(Status.ACTIVE);
					cityInfoRepository.save(c);
				}

			}
		}
	}

}
