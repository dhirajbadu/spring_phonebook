<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Name *</label>
            <input type="text" class="form-control" value="${clientInfo.name}" name="name" placeholder="name" required>
            <p class="form-error">${clientInfoError.name}</p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Contact Number*</label>
            <input type="text" class="form-control" value="${clientInfo.contact}" name="contact"
                   placeholder="contact number" required>
            <p class="form-error">${clientInfoError.contact}</p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Email Address*</label>
            <input type="text" class="form-control" value="${clientInfo.email}" name="email" placeholder="email address"
                   required>
            <p class="form-error">${clientInfoError.email}</p>
        </div>
    </div>

</div>

<div class="row">
    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">House Number *</label>
            <input type="text" class="form-control" value="${clientInfo.house_no}" name="house_no"
                   placeholder="House Number" required>
            <p class="form-error">${clientInfoError.house_no}</p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">City *</label>
            <select name="cityInfoId" class="form-control select2 select2-hidden-accessible"
                    data-placeholder="Select City" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <c:forEach items="${cityList}" var="city">
                    <c:choose>
                        <c:when test="${city.cityId eq clientInfo.cityInfoId}">
                            <option selected="selected" value="${city.cityId}">${city.cityName}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${city.cityId}">${city.cityName}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <p class="form-error">${clientInfoError.cityInfoId}</p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Street *</label>
            <input type="text" class="form-control" value="${clientInfo.street}" name="street" placeholder="street"
                   required>
            <p class="form-error">${clientInfoError.street}</p>
        </div>
    </div>

</div>

<div class="row">

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">Branch *</label>
            <select name="branchId" class="form-control select2 select2-hidden-accessible"
                    data-placeholder="Select Branch" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <c:forEach items="${branchList}" var="branch">
                    <c:choose>
                        <c:when test="${branch.branchId eq clientInfo.branchId}">
                            <option selected="selected" value="${branch.branchId}">${branch.code}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${branch.branchId}">${branch.code}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <p class="form-error">${clientInfoError.branchId}</p>
        </div>
    </div>

    <div class="col-md-4">
        <div class="form-group">
            <label class="control-label">ClientType *</label>
            <select name="clientTypeId" class="form-control select2 select2-hidden-accessible"
                    data-placeholder="Select ClientType" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                <c:forEach items="${clientTypeList}" var="clientType">
                    <c:choose>
                        <c:when test="${clientType.clientTypeInfoId eq clientInfo.clientTypeId}">
                            <option selected="selected"
                                    value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                        </c:when>
                        <c:otherwise>
                            <option value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                        </c:otherwise>
                    </c:choose>
                </c:forEach>
            </select>
            <p class="form-error">${clientInfoError.clientTypeId}</p>
        </div>
    </div>

</div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

