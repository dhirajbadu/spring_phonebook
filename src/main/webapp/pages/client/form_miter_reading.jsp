<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<form action="${pageContext.request.contextPath}/clientunit/save" method="post" modelAttribute="clientUnitInfo" autocomplete="off">
<div class="box box-info">
    <div class="box-header with-border">
        <h3 class="box-title">Miter Reading</h3>
    </div>
    <div class="box-body">


            <input type="hidden" name="clientInfoId" value="${clientInfo.clientInfoId}" required/>
            <input type="hidden" name="callBackUrl" value="clientinfo/show/${clientInfo.clientInfoId}"/>
            <input type="hidden" name="hascallBackUrl" value="1"/>
            <p class="form-error">${clientUnitInfoError.clientInfoId}</p>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">From Date *</label>
                        <input type="text" class="form-control datepicker" onkeypress="return false;"
                               onkeyup="return false;"
                               value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.fromDate}"/>"
                               name="fromDate"
                               placeholder="From Date" required>
                        <p class="form-error">${clientUnitInfoError.fromDate}</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">To Date *</label>
                        <input type="text" class="form-control datepicker" onkeypress="return false;"
                               onkeyup="return false;"
                               value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.toDate}"/>"
                               name="toDate"
                               placeholder="To Date" required>
                        <p class="form-error">${clientUnitInfoError.toDate}</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Month *</label>
                        <input type="text" class="form-control datepickermonth" onkeypress="return false;"
                               onkeyup="return false;"
                               value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.ofTheMonth}"/>"
                               name="ofTheMonth" placeholder="Of the month" required>
                        <p class="form-error">${clientUnitInfoError.ofTheMonth}</p>
                    </div>
                </div>

                <div class="col-md-4">
                    <div class="form-group">
                        <label class="control-label">Unit Record *</label>
                        <input type="text" id="txtboxToFilterNumber" class="form-control"
                               value="${clientUnitInfo.totalUnitRecord}" name="totalUnitRecord"
                               placeholder="Total Unit Record"
                               required>
                        <p class="form-error">${clientUnitInfoError.totalUnitRecord}</p>
                    </div>
                </div>
            </div>


    </div>

    <!-- /.box-body -->
    <div class="box-footer clearfix">
        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
        <button type="submit" class="btn btn-primary">Save changes</button>
    </div>
</div>
</form>