<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="client details">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Client Details</h3>
                                    <%-- <sec:authorize access="hasRole('Role_ClientInfo_Create')">
                                         <a href="${pageContext.request.contextPath}/clientinfo/add"
                                            class="btn btn-info btn-sm btn-flat pull-right"><span
                                                 class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                     </sec:authorize>--%>

                                <sec:authorize access="hasRole('Role_ClientInfo_View')">
                                    <a href="${pageContext.request.contextPath}/clientinfo/list"
                                       class="btn btn-primary btn-sm btn-flat pull-right"><span
                                            class="glyphicon glyphicon-list-alt"></span>&nbsp; List</a>
                                </sec:authorize>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <c:choose>
                                    <c:when test="${clientInfo ne null}">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <c:choose>
                                                        <c:when test="${clientInfo.clientProfileInfoDTO ne null}">


                                                            <c:choose>
                                                                <c:when test="${clientInfo.clientProfileInfoDTO.photoName ne null}">
                                                                    <a href="${pageContext.request.contextPath}/clientapplication/show/${clientInfo.clientProfileInfoId}" target="_blank">
                                                                        <img class="img-circle" src="${pageContext.request.contextPath}/image/${clientInfo.clientProfileInfoDTO.photoName}" style="width:200px">
                                                                        </br>
                                                                        <span class="btn-default text-center">view application</span>
                                                                    </a>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <a href="${pageContext.request.contextPath}/clientapplication/show/${clientInfo.clientProfileInfoId}" target="_blank">

                                                                        <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg" style="width:200px">
                                                                        </br>
                                                                        <span class="btn-default text-center">view application</span>
                                                                    </a>
                                                                </c:otherwise>
                                                            </c:choose>

                                                        </c:when>
                                                        <c:otherwise>
                                                            <a href="${pageContext.request.contextPath}/clientapplication/show/${clientInfo.clientProfileInfoId}" target="_blank">
                                                                <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg" style="width:200px">
                                                                </br>
                                                                <span class="btn-default text-center">view application</span>
                                                            </a>
                                                        </c:otherwise>
                                                    </c:choose>

                                                </p>
                                            </div>

                                            <div class="col-md-4 pull-right">
                                                <p class="well well-sm"><label>Approved Date :</label> <i
                                                        class="fa fa-ca"></i>&nbsp;<label><fmt:formatDate
                                                        pattern="yyyy-MM-dd" value="${clientInfo.createdDate}"/></label>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Name</label>
                                                <p class="well well-sm">${clientInfo.name}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Client Id</label>
                                                <p class="well well-sm">${clientInfo.code}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Contact</label>
                                                <p class="well well-sm">${clientInfo.contact}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Email Address</label>
                                                <p class="well well-sm">${clientInfo.email}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>House Num</label>
                                                <p class="well well-sm">${clientInfo.house_no}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Type</label>
                                                <p class="well well-sm">${clientInfo.clientTypeTitle}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>City</label>
                                                <p class="well well-sm">${clientInfo.cityInfoName}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Street</label>
                                                <p class="well well-sm">${clientInfo.street}</p>
                                            </div>
                                            <div class="col-md-4">

                                                <c:choose>
                                                    <c:when test="${clientInfo.branchId ne null}">
                                                        <label>Zone</label>
                                                        <p class="well well-sm">${clientInfo.branchName}</p>
                                                    </c:when>
                                                    <c:otherwise>
                                                        <form action="${pageContext.request.contextPath}/clientinfo/save/branch"
                                                              method="post">
                                                            <input type="hidden" name="clientId"
                                                                   value="${clientInfo.clientInfoId}">
                                                            <div class="well well-sm">
                                                                <div class="form-group">
                                                                    <label class="control-label">Add Zone *</label>
                                                                    <select name="branchId"
                                                                            class="form-control select2 select2-hidden-accessible"
                                                                            data-placeholder="Select Branch"
                                                                            style="width: 100%;" tabindex="-1"
                                                                            aria-hidden="true" required>
                                                                        <c:forEach items="${branchList}" var="branch">
                                                                            <option value="${branch.branchId}">${branch.code}</option>
                                                                        </c:forEach>
                                                                    </select>
                                                                </div>

                                                                <div class="modal-footer">
                                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                                           value="${_csrf.token}"/>
                                                                    <button type="submit" class="btn btn-primary">Save
                                                                        changes
                                                                    </button>
                                                                </div>

                                                            </div>
                                                        </form>
                                                    </c:otherwise>
                                                </c:choose>

                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-lg-12">
                                                <span class="pull-right"><a
                                                        href="#"><p class="text-blue text-bold">Approved By : ${clientInfo.createByName}</p></a> </span>
                                            </div>
                                        </div>

                                        <%--<div class="row margin">
                                            <div class="col-md-4 pull-left"><sec:authorize
                                                    access="hasRole('Role_ClientProfileInfo_View')"><a
                                                    href="${pageContext.request.contextPath}/clientapplication/show/${clientInfo.clientProfileInfoId}"
                                                    class="btn btn-default btn-lg pull-left"><span
                                                    class="glyphicon glyphicon-floppy-disk"></span>&nbsp;profile</a></sec:authorize>
                                            </div>
                                                &lt;%&ndash;<div class="col-md-4 pull-right"><sec:authorize access="hasRole('Role_ClientInfo_Update')"><a href="${pageContext.request.contextPath}/clientinfo/edit/${clientInfo.clientInfoId}" class="btn btn-warning btn-sm pull-right"><span class="glyphicon glyphicon-pencil"></span>&nbsp;edit</a></sec:authorize></div>&ndash;%&gt;
                                        </div>--%>


                                    </c:when>
                                    <c:otherwise>
                                        <p class="form-error">clientNotFound</p>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
            <section class="content">
                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">Miter Reading Details</h3>
                            </div>
                            <div class="box-body">

                                <div class="">
                                    <table id="ajaxDataTable"
                                           url="${pageContext.request.contextPath}/ajax/clientunit/list/${clientInfo.clientInfoId}"
                                           class="table table-bordered table-hover table-striped">
                                        <thead>
                                        <tr>
                                            <th>SN</th>
                                            <th>From</th>
                                            <th>To</th>
                                            <th>Month</th>
                                            <th>Unit</th>
                                            <th>Amount</th>
                                            <th>Total Record</th>
                                            <th>Bill Type</th>
                                        </tr>
                                        </thead>
                                        <tbody id="myData">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
                <%--
                            <section class="content">
                                    &lt;%&ndash;setup miter&ndash;%&gt;
                                <div class="row">
                                    <div class="col-lg-12">
                                        <jsp:include page="/pages/client/form_miter_setup.jsp"/>
                                    </div>
                                </div>
                            </section>

                            <section class="content">
                                    &lt;%&ndash;reading miter&ndash;%&gt;
                                <div class="row">
                                    <div class="col-lg-12">
                                        <jsp:include page="/pages/client/form_miter_reading.jsp"/>
                                    </div>
                                </div>
                            </section>--%>
        </div>
    </jsp:body>

</t:genericpage>

<script>
    $(document).ready(function () {

        $(function () {
            $.fn.dataTable.ext.errMode = 'none';

            $('#ajaxDataTable').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ordering: true,
                lengthChange: true,
                order: [0, "desc"],
                ajax: $('#ajaxDataTable').attr('url'),
                columnDefs: [{
                    targets: 7,
                    render: function (data, type, row) {

                        var result = "";

                        if (row[8] == "SetUp") {
                            result = '<span class="label label-default">SetUp</span>&nbsp;';
                        } else if (row[8] == "Changed") {
                            result = '<span class="label label-warning">Changed</span>&nbsp;';
                        } else if (row[8] == "Reading") {
                            result = '<span class="label label-primary">Reading</span>&nbsp;';
                        }

                        if (row[7] === "Na") {
                            result += '<a href="${pageContext.request.contextPath}/clientunit/invoice/add/' + row[0] + '" target="_blank" ><span class="label label-info">Generate Bill</span></a>';
                        } else if (row[7] === "Unpaid") {
                            result += '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" target="_blank" ><span class="label label-danger">Unpaid</span></a>';
                        } else if (row[7] === "Partial") {
                            result += '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" target="_blank"><span class="label label-warning">Partial</span></a>';
                        } else if (row[7] === "Full") {
                            result += '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" target="_blank" ><span class="label label-success">Fullpaid</span></a>';
                        }

                        if (row[7] === "Unpaid" || row[7] === "Partial" || row[7] === "Full") {
                            result += '&nbsp;<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" class="btn btn-default" target="_blank" >Show Bill</a>';
                        }

                        return result;
                    }
                }]
            });
        });

    });
</script>