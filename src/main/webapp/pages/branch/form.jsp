<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${branch.title}" name="title" placeholder="zone title" required>
                <p class="form-error">${branchError.title}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Code *</label>
                <input type="text" class="form-control" value="${branch.code}" name="code" placeholder="zone code" required>
                <p class="form-error">${branchError.code}</p>
            </div>
        </div>
    </div>
</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

