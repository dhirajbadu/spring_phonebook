

<div class="row">
    <div class="col-lg-12 col-md-offset-5">
        <label>Tikapur Sana Sahakari Khanepani Limited</label><br>
        <label>Tikapur , Kailali , Nepal</label><br>
        <label>014433222</label>
    </div>
</div>

<div class="row">
    <div class="col-md-6">

        <label>Name : ${clientUnitInfo.clientInfoName}</label><br>
        <label>Code : ${clientUnitInfo.clientInfoCode}</label><br>
    </div>

    <div class="col-md-6">
        <label class="pull-right">Invoice No : #${invoiceInfo.invoiceNumber}</label><br>
        <label class="pull-right">Date : <fmt:formatDate pattern="MMM dd, yyyy" value="${payment.paymentDate}"/></label><br>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">
        <table class="table">
            <thead>
            <tr>
                <th>Month</th>
                <th>Paid Amount</th>
                <th>Due Amount</th>
            </tr>
            </thead>

            <tbody>
            <tr>
                <td>${clientUnitInfo.ofTheMonthStr}</td>
                <td><fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${payment.amount}"/></td>
                <td><fmt:formatNumber type="number" maxFractionDigits="3" groupingUsed="true" value="${invoiceInfo.grandTotalAmount - invoiceInfo.totalPaidAmount}"/></td>
            </tr>
            </tbody>

        </table>
    </div>
</div>
