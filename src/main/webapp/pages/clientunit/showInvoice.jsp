<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="invoice details">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                <div class="alert alert-success alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                    <strong>${message}</strong>
                </div>
                </c:if>

                <c:if test="${not empty error}">
                <div class="alert alert-danger alert-dismissable">
                    <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                    <strong>${error}</strong>
                </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Invoice Details</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_ClientInfo_View')">
                                        <a href="${pageContext.request.contextPath}/clientinfo/list"
                                           class="btn btn-info btn-sm btn-flat pull-right"><span
                                                class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-lg-12">
                                        <c:if test="${invoiceInfo.billType.value eq 'Unpaid'}">
                                            <span class="label label-danger text-center col-md-offset-5"><strong>Unpaid</strong></span>
                                        </c:if>
                                        <c:if test="${invoiceInfo.billType.value eq 'Partial'}">
                                            <span class="label label-warning text-center col-md-offset-5"><strong>Partially paid</strong></span>
                                        </c:if>
                                        <c:if test="${invoiceInfo.billType.value eq 'Full'}">
                                            <span class="label label-success text-center col-md-offset-5"><strong>Fully Paid</strong></span>
                                        </c:if>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-5 pull-left">
                                        <label>Client Name :</label>
                                        <p class="well well-sm">${clientUnitInfo.clientInfoName}</p>
                                        <label>Client Code :</label>
                                        <p class="well well-sm">${clientUnitInfo.clientInfoCode}</p>
                                    </div>
                                    <div class="col-md-4 pull-right">
                                        <div class="form-group">
                                            <label>Invoice Number</label>
                                            <p class="well well-sm">${invoiceInfo.invoiceNumber}</p>
                                            <label class="control-label">Invoice Date</label>
                                            <p class="well well-sm"><fmt:formatDate pattern="MMM dd, yyyy"
                                                                                    value="${invoiceInfo.invoiceDate}"/></p>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-12">
                                        <table class="table">
                                            <thead>
                                            <tr>
                                                <th>Month</th>
                                                <th>Unit</th>
                                                <th>Amount</th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            <tr>
                                                <td>${clientUnitInfo.ofTheMonthStr}</td>
                                                <td>${clientUnitInfo.currentUnit}</td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.netAmount}"/></td>
                                            </tr>
                                            </tbody>
                                            <tfoot>

                                            <tr>
                                                <th>Fine</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.fineAmount}"/></td>
                                            </tr>

                                            <c:if test="${invoiceInfo.additionalChargeDTOList ne null}">
                                            <c:forEach var="chargeTemplate" items="${invoiceInfo.additionalChargeDTOList}" varStatus="i">
                                                <tr>
                                                    <th>${chargeTemplate.title}</th>
                                                    <td></td>
                                                    <td>
                                                        <fmt:formatNumber type="number" maxFractionDigits="3"
                                                                          groupingUsed="true"
                                                                          value="${chargeTemplate.amount}"/>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </c:if>

                                            <tr>
                                                <th>Discount</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.discountAmount}"/></td>
                                            </tr>
                                            <tr>
                                                <th>Tax</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.taxAmount}"/></td>
                                            </tr>
                                            <tr>
                                                <th>Grand Total</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.grandTotalAmount}"/></td>
                                            </tr>

                                            <tr>
                                                <th>Paid Amount</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.totalPaidAmount}"/></td>
                                            </tr>

                                            <tr>
                                                <th>Due Amount</th>
                                                <td></td>
                                                <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                      groupingUsed="true"
                                                                      value="${invoiceInfo.grandTotalAmount - invoiceInfo.totalPaidAmount}"/></td>
                                            </tr>

                                            </tfoot>
                                        </table>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6 pull-left">
                                        <div class="panel panel-color panel-primary">
                                            <div class="panel-heading">
                                                <h3 class="panel-title">Remarks</h3>
                                            </div>
                                            <div class="panel-body">
                                                <p> ${invoiceInfo.remarks}</p>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">

                                    <div class="col-xs-4 pull-right">
                                        <a href="${pageContext.request.contextPath}/export/invoice/pdf?invoiceId=${invoiceInfo.invoiceInfoId}"
                                           class="btn btn-primary" target="_blank" style="margin-right: 5px;"><i
                                                class="fa fa-file-pdf-o"></i> Generate PDF</a>
                                    </div>

                                    <%--<div class="col-xs-4 pull-right">
                                        <a href="${pageContext.request.contextPath}/export/invoice/excel?invoiceId=${invoiceInfo.invoiceInfoId}"
                                           class="btn btn-info" target="_blank" style="margin-right: 5px;"><i
                                                class="fa fa-file-excel-o"></i> Generate Excel</a>
                                    </div>--%>

                                </div>

                                <div class="row" style="margin-top: 5px;">
                                    <div class="col-lg-12">
                                        <div class="panel panel-color panel-success">
                                            <div class="panel-heading">
                                                <h3 class="panel-title" style="margin-left: 40%"><b>Payment Details</b>
                                                </h3>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-lg-12">
                                        <c:if test="${invoiceInfo.paymentList ne null}">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Date</th>
                                                    <th>Amount</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <c:forEach items="${invoiceInfo.paymentList}" var="payment" varStatus="i">
                                                    <tr>
                                                        <td><fmt:formatDate pattern="MMM dd, yyyy"
                                                                            value="${payment.paymentDate}"/></td>
                                                        <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                              groupingUsed="true"
                                                                              value="${payment.amount}"/></td>
                                                        <td><a href="${pageContext.request.contextPath}/export/payment/receipt?invoiceInfoId=${invoiceInfo.invoiceInfoId}&paymentInfoId=${payment.paymentInfoId}" target="_blank" class="btn btn-info printPayment" id="paymentReceipt${i.index}"><i class="fa fa-print"></i>&nbsp;print</a></td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </c:if>
                                    </div>
                                </div>

                                <c:if test="${invoiceInfo.billType.value ne 'Full'}">
                                    <div class="row">
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="panel panel-color panel-info">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title" style="margin-left: 40%"><b>Make Payment</b>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <form action="${pageContext.request.contextPath}/invoice/payment"
                                                          method="post" modelAttribute="payment" autocomplete="off">

                                                        <input type="hidden" name="invoiceId" value="${invoiceInfo.invoiceInfoId}">
                                                        <div class="box-body">

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Payment Date *</label>
                                                                        <input type="text" class="form-control datepicker"
                                                                               onkeypress="return false;"
                                                                               onkeyup="return false;"
                                                                               value="<fmt:formatDate pattern="MM/dd/yyyy" value="${paymentInfo.paymentDate}"/>"
                                                                               name="paymentDate"
                                                                               placeholder="From Date" required>
                                                                        <p class="form-error">${paymentInfoError.paymentDate}</p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Amount *</label>
                                                                        <input type="text" id="txtboxToFilterNumber"
                                                                               class="form-control"
                                                                               value="${paymentInfo.amount}"
                                                                               name="amount" onkeypress='return validate(event);'
                                                                               placeholder="Total Unit Record"
                                                                               required>
                                                                        <p class="form-error">${paymentInfoError.amount}</p>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <input type="hidden" name="${_csrf.parameterName}"
                                                                   value="${_csrf.token}"/>
                                                            <button type="submit" class="btn btn-primary pull-left">Save Payment</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-lg-6">
                                            <div class="row">
                                                <div class="col-lg-12">
                                                    <div class="panel panel-color panel-warning">
                                                        <div class="panel-heading">
                                                            <h3 class="panel-title" style="margin-left: 40%"><b>Make Fine</b>
                                                            </h3>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12">
                                                    <form action="${pageContext.request.contextPath}/invoice/fine"
                                                          method="post" modelAttribute="payment" autocomplete="off">

                                                        <input type="hidden" name="invoiceId" value="${invoiceInfo.invoiceInfoId}">
                                                        <div class="box-body">

                                                            <div class="row">
                                                                <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="control-label">Fine Amount *</label>
                                                                        <input type="text" id="txtboxToFilterNumber"
                                                                               class="form-control"
                                                                               name="amount" onkeypress='return validate(event);'
                                                                               placeholder="Enter Fine Amount"
                                                                               required>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                        </div>

                                                        <div class="modal-footer">
                                                            <input type="hidden" name="${_csrf.parameterName}"
                                                                   value="${_csrf.token}"/>
                                                            <button type="submit" class="btn btn-primary pull-left">Save Fine</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </c:if>

                            </div>

                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                </div>
        </section>
        </div>


    </jsp:body>

</t:genericpage>