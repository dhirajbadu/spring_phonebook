<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="initiate clientUnit add">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Initiate Client Unit Add</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_ClientUnitInfo_View')">
                                        <a href="${pageContext.request.contextPath}/clientunit/list" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                               <form action="${pageContext.request.contextPath}/clientunit/initiate/save" method="post" modelAttribute="clientUnitInfo" autocomplete="off">

                                   <div class="box-body">

                                       <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label">Client *</label>
                                                   <select class="choose1 form-control" name="clientInfoId" required></select>
                                                   <p class="form-error">${clientUnitInfoError.clientInfoId}</p>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label">Date *</label>
                                                   <input type="text" class="form-control datepicker" onkeypress="return false;" onkeyup="return false;"
                                                          value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.fromDate}"/>" name="fromDate"
                                                          placeholder="From Date" required>
                                                   <p class="form-error">${clientUnitInfoError.fromDate}</p>
                                               </div>
                                           </div>
                                       </div>

                                       <div class="row">
                                           <div class="col-md-4">
                                               <div class="form-group">
                                                   <label class="control-label">Unit Record *</label>
                                                   <input type="text" id="txtboxToFilterNumber" class="form-control"
                                                          value="${clientUnitInfo.totalUnitRecord}" name="totalUnitRecord" placeholder="Total Unit Record"
                                                          required>
                                                   <p class="form-error">${clientUnitInfoError.totalUnitRecord}</p>
                                               </div>
                                           </div>
                                       </div>

                                   </div>
                                   <!-- /.box-body -->
                                   <div class="modal-footer">
                                       <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                       <button type="submit" class="btn btn-primary">Save changes</button>
                                   </div>

                               </form>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

<script>

    $(document).ready(function() {
        $("#txtboxToFilterNumber").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110]) !== -1 ||
                // Allow: Ctrl+A, Command+A
                (e.keyCode === 65 && (e.ctrlKey === true || e.metaKey === true)) ||
                // Allow: home, end, left, right, down, up
                (e.keyCode >= 35 && e.keyCode <= 40)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });

</script>

<script>
    $(document).ready(function() {

        $(".choose1").select2({
            ajax: {
                url: '${pageContext.request.contextPath}/ajax/clientinfo/search',
                dataType: 'json',
                headers : {'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')},
                delay: 250,
                type: 'GET',
                data: function (params) {
                    return {
                        term: params.term // search term
                        /* page: params.page*/
                    };
                },
                processResults: function (data , params) {
                    params.page = params.page || 1;
                    var arr = [];
                    $.each(data.detail, function (index, value) {

                        arr.push({
                            id: value.clientInfoId,
                            text: value.name + ' - ' + value.code
                        })

                    });



                    return {
                        results: arr/*,
                         pagination: {
                         more: (params.page * 1) < 2
                         }*/
                    };
                },
                cache: true
            },
            escapeMarkup: function (markup) { return markup; },
            minimumInputLength: 1,
            placeholder: "Search Customer by Name or Code or Contact"
        });
    });
</script>
