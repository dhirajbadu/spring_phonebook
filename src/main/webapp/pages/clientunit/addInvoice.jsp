<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="invoice add">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Invoice Add</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_ClientInfo_View')">
                                        <a href="${pageContext.request.contextPath}/clientinfo/list"
                                           class="btn btn-info btn-sm btn-flat pull-right"><span
                                                class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <form action="${pageContext.request.contextPath}/clientunit/invoice/save"
                                      method="post" modelAttribute="invoiceinfo" novalidate="novalidate" autocomplete="off">

                                    <input type="hidden" name="clientUnitInfoId" value="${clientUnitInfo.clientUnitInfoId}">
                                    <input type="hidden" name="clientInfoId" value="${clientUnitInfo.clientInfoId}">
                                    <input type="hidden" name="invoiceNumberId" value="${codeNumber.id}">

                                    <div class="row">
                                        <div class="col-md-5 pull-left">
                                            <label>Client Name :</label>
                                            <p class="well well-sm">${clientUnitInfo.clientInfoName}</p>
                                            <label>Client Code :</label>
                                            <p class="well well-sm">${clientUnitInfo.clientInfoCode}</p>
                                        </div>
                                        <div class="col-md-4 pull-right">
                                            <div class="form-group">
                                                <label>Invoice Number # <b>${codeNumber.numStr}</b></label><br>
                                                <label class="control-label">Invoice Date *</label>
                                                <input type="text" class="form-control datepicker"
                                                       onkeypress="return false;" onkeyup="return false;"
                                                       value="<fmt:formatDate pattern="MM/dd/yyyy" value="${invoiceInfo.invoiceDate}"/>"
                                                       name="invoiceDate"
                                                       placeholder="Invoice Date" required>
                                                <p class="form-error">${invoiceInfoError.invoiceDate}</p>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-lg-12">
                                            <table class="table">
                                                <thead>
                                                <tr>
                                                    <th>Month</th>
                                                    <th>Unit</th>
                                                    <th>Amount</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                <tr>
                                                    <td>${clientUnitInfo.ofTheMonthStr}</td>
                                                    <td>${clientUnitInfo.currentUnit}</td>
                                                    <td><fmt:formatNumber type="number" maxFractionDigits="3"
                                                                          groupingUsed="true" value="${amount}"/><%--<input type="number" class="form-control" style="width: 70%"
                                                               value="${0}" name="netAmount" placeholder="Net Amount"
                                                               required>--%></td>
                                                </tr>
                                                </tbody>
                                                <tfoot>
                                                <tr>
                                                    <th>Fine</th>
                                                    <td></td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${invoiceInfo.fineAmount ne null}">
                                                                <input type="number" class="form-control" style="width: 70%" value="${invoiceInfo.fineAmount}" name="fineAmount" placeholder="Fine Amount" required>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <input type="number" class="form-control" style="width: 70%" value="${0}" name="fineAmount" placeholder="Fine Amount" required>
                                                            </c:otherwise>
                                                        </c:choose>
                                                        <br><p class="form-error">${invoiceInfoError.fine}</p>
                                                    </td>
                                                </tr>

                                                <c:forEach var="chargeTemplate" items="${invChargeTemplateList}" varStatus="i">
                                                    <tr>
                                                        <th>${chargeTemplate.title}</th>
                                                        <td></td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${invoiceInfo.additionalChargeDTOList ne null}">
                                                                    <input type="number" style="width: 70%" class="form-control" placeholder="${chargeTemplate.title}" name="additionalChargeDTOList[${i.index}].amount" value="${invoiceInfo.additionalChargeDTOList[i.index].amount}">
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <input type="number" style="width: 70%" class="form-control" placeholder="${chargeTemplate.title}" name="additionalChargeDTOList[${i.index}].amount" value="${0}">
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <input type="hidden" name="additionalChargeDTOList[${i.index}].chargeTemplateId" value="${chargeTemplate.invoiceChargeTemplateId}">
                                                            <input type="hidden" name="additionalChargeDTOList[${i.index}].title" value="${chargeTemplate.title}">
                                                        </td>
                                                    </tr>
                                                </c:forEach>

                                                <tr>
                                                    <th>Discount</th>
                                                    <td></td>
                                                    <td>
                                                        <c:choose>
                                                            <c:when test="${invoiceInfo.discountAmount ne null}">
                                                                <input type="number" class="form-control" style="width: 70%"
                                                                       value="${invoiceInfo.discountAmount}" name="discountAmount"
                                                                       placeholder="Discount Amount" required>
                                                            </c:when>
                                                            <c:otherwise>
                                                                <input type="number" class="form-control" style="width: 70%"
                                                                       value="${0}" name="discountAmount"
                                                                       placeholder="Discount Amount" required>
                                                            </c:otherwise>
                                                        </c:choose>

                                                        <br><p class="form-error">${invoiceInfoError.discount}</p>
                                                    </td>
                                                </tr>
                                                </tfoot>
                                            </table>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6 pull-left">
                                            <div class="form-group">
                                                <br>
                                                <label class="control-label">Remarks *</label>
                                                <textarea class="form-control" name="remarks" rows="8" cols="30"
                                                          maxlength="490">${invoiceInfo.remarks}</textarea>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="modal-footer">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>
