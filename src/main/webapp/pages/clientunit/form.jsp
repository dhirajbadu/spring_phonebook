<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Client *</label>
                <select class="choose1 form-control" name="clientInfoId" required></select>
                <p class="form-error">${clientUnitInfoError.clientInfoId}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">From Date *</label>
                <input type="text" class="form-control datepicker" onkeypress="return false;" onkeyup="return false;"
                       value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.fromDate}"/>" name="fromDate"
                       placeholder="From Date" required>
                <p class="form-error">${clientUnitInfoError.fromDate}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">To Date *</label>
                <input type="text" class="form-control datepicker" onkeypress="return false;" onkeyup="return false;"
                       value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.toDate}"/>" name="toDate"
                       placeholder="To Date" required>
                <p class="form-error">${clientUnitInfoError.toDate}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Month *</label>
                <input type="text" class="form-control datepickermonth" onkeypress="return false;" onkeyup="return false;"
                       value="<fmt:formatDate pattern="MM/dd/yyyy" value="${clientUnitInfo.ofTheMonth}"/>"
                       name="ofTheMonth" placeholder="Of the month" required>
                <p class="form-error">${clientUnitInfoError.ofTheMonth}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Unit Record *</label>
                <input type="text" id="txtboxToFilterNumber" class="form-control"
                       value="${clientUnitInfo.totalUnitRecord}" name="totalUnitRecord" placeholder="Total Unit Record"
                       required>
                <p class="form-error">${clientUnitInfoError.totalUnitRecord}</p>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

