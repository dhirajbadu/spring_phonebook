<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="block list">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Block List</h3>
                                <sec:authorize access="hasAnyRole('System' , 'Admin')">
                                    <a href="${pageContext.request.contextPath}/block/add" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                </sec:authorize>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="table3" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Title</th>
                                                    <th>Code</th>
                                                    <th>Ward</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <c:forEach var="block" items="${blockList}" varStatus="i">
                                                    <tr>
                                                        <td>${i.index + 1}</td>
                                                        <td>${block.title}</td>
                                                        <td>${block.code}</td>
                                                        <td>
                                                            <c:choose>
                                                                <c:when test="${block.wardInfoDTO ne null}">
                                                                    ${block.wardInfoDTO.code}
                                                                </c:when>
                                                                <c:otherwise>
                                                                    NA
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </td>
                                                        <td><a href="${pageContext.request.contextPath}/block/edit?blockInfoId=${block.blockInfoId}" class="btn btn-primary"><span class="glyphicon glyphicon-pencil"></span>&nbsp; Edit</a> &nbsp;<a href="${pageContext.request.contextPath}/block/delete?blockInfoId=${block.blockInfoId}" class="btn btn-danger"><span class="glyphicon glyphicon-trash"></span>&nbsp; Delete</a> </td></td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>
