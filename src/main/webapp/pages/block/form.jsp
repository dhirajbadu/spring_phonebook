<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${block.title}" name="title" placeholder="block title" required>
                <p class="form-error">${blockError.title}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Code *</label>
                <input type="text" class="form-control" value="${block.code}" name="code" placeholder="block code" required>
                <p class="form-error">${blockError.code}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Ward *</label>

                <select name="wardInfoId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select ward" style="width: 100%;" tabindex="-1" aria-hidden="true"
                        required>
                    <c:forEach items="${wardList}" var="ward">
                        <c:choose>
                            <c:when test="${ward.wardInfoId eq block.wardInfoId}">
                                <option selected="selected" value="${ward.wardInfoId}">${ward.code}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${ward.wardInfoId}">${ward.code}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>

                <p class="form-error">${blockError.wardInfId}</p>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

