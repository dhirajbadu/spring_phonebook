<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<style type="text/css">
    .btn-secondary:not(:disabled):not(.disabled).active, .btn-secondary:not(:disabled):not(.disabled):active, .show>.btn-secondary.dropdown-toggle {
        color: #fff;
        background-color: #0496ce;
        border-color: #4e555b;
    }
</style>

<div class="box-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${invChargeTemplate.title}" name="title"
                       placeholder=" title" required>
                <p class="form-error">${invChargeTemplateError.title}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Code *</label>
                <input type="text" class="form-control" value="${invChargeTemplate.code}" name="code"
                       placeholder=" code" required>
                <p class="form-error">${invChargeTemplateError.code}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Minimum Amount *</label>
                <input type="number" class="form-control" value="${invChargeTemplate.minAmount}" name="minAmount"
                       placeholder=" minAmount" required>
                <p class="form-error">${invChargeTemplateError.minAmount}</p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">isRequired *</label><br>

                <div class="btn-group btn-group-toggle" data-toggle="buttons">
                    <label class="btn btn-secondary">
                        <input type="checkbox" name="required" value="true" autocomplete="off"> Required
                    </label>
                </div>

            </div>
        </div>

        <div class="col-md-6">
            <label class="control-label">Charge Type *</label><br>

            <div class="btn-group btn-group-toggle" data-toggle="buttons">

                <c:set var="chargeType" value="${invChargeTemplate.chargeType}"></c:set>

                <c:choose>
                    <c:when test="${fn:containsIgnoreCase(chargeType, 'SETUP')}">
                        <label class="btn btn-secondary active">
                            <input type="checkbox" name="chargeType" value="SETUP" autocomplete="off" checked> Setup
                        </label>
                    </c:when>
                    <c:otherwise>
                        <label class="btn btn-secondary">
                            <input type="checkbox" name="chargeType" value="SETUP" autocomplete="off"> Setup
                        </label>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${fn:containsIgnoreCase(chargeType, 'CHANGE')}">
                        <label class="btn btn-secondary active">
                            <input type="checkbox" autocomplete="off" name="chargeType" value="CHANGE" checked> Change
                        </label>
                    </c:when>
                    <c:otherwise>
                        <label class="btn btn-secondary">
                            <input type="checkbox" autocomplete="off" name="chargeType" value="CHANGE"> Change
                        </label>
                    </c:otherwise>
                </c:choose>

                <c:choose>
                    <c:when test="${fn:containsIgnoreCase(chargeType, 'READING')}">
                        <label class="btn btn-secondary active">
                            <input type="checkbox" autocomplete="off" name="chargeType" value="READING" checked> Reading
                        </label>
                    </c:when>
                    <c:otherwise>
                        <label class="btn btn-secondary">
                            <input type="checkbox" autocomplete="off" name="chargeType" value="READING"> Reading
                        </label>
                    </c:otherwise>
                </c:choose>
            </div>
            <p class="form-error">${invChargeTemplateError.chargeType}</p>
        </div>
    </div>
</div>
<!-- /.box-body -->
<div class="box-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary"><i class="fa fa-save"></i> &nbsp;Save changes</button>
</div>