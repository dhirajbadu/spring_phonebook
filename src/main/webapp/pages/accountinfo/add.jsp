<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage title="Add Account Info">
	<jsp:body>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">

                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert"
							aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert"
							aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>

                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Add Account Info</h3>
                            </div>
                            <!-- /.box-header -->
                            <form
								action="${pageContext.request.contextPath}/accountinfo/save"
								method="post" modelAttribute="accountInfo">
                                <div class="box-body">

                                    <div class="form-group">
                                        <label class="control-label">Account Number</label>
                                        <input type="text"
											class="form-control" name="accnum"
											placeholder="Account Number" required>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Total Amount</label>
                                        <input type="text"
											class="form-control" name="totalAmount"
											placeholder="Total Amount" required>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Paid Amount</label>
                                        <input type="text"
											class="form-control" name="paidAmount"
											placeholder="Paid Amount" required>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Account Type</label>
                                        <select name="accountType"
											class="form-control select2 select2-hidden-accessible"
											style="width: 100%;" tabindex="-1" aria-hidden="true"
											required>
						<c:if test="${fn:length(accountType) gt 0}">

							<option value="" selected disabled>Select Account Type</option>

							<c:forEach var="accountType" items="${accountType}">
								<option value="${accountType}">${accountType}</option>
							</c:forEach>
						</c:if>

					</select>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>

						<div class="form-group">
                                        <label class="control-label">Client Info</label>
                                        <select name="clientInfoId"
											class="form-control select2 select2-hidden-accessible"
											style="width: 100%;" tabindex="-1" aria-hidden="true"
											required>
						<c:if test="${fn:length(clientInfoList) gt 0}">

							<option value="" selected disabled>Select Account Type</option>

							<c:forEach var="clientInfo" items="${clientInfoList}">
								<option value="${clientInfo.clientInfoId}">${clientInfo.name}</option>
							</c:forEach>
						</c:if>

					</select>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>


                                </div>
                                <!-- /.box-body -->
                                <div class="modal-footer">
                                    <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <button type="submit"
										class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </jsp:body>

</t:genericpage>


