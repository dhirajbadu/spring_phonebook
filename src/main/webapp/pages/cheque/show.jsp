<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 3/10/18
  Time: 2:14 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="cheque details">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Cheque Details</h3>
                                <sec:authorize access="hasRole('Role_Cheque_Create')">
                                    <a href="${pageContext.request.contextPath}/cheque/add" class="btn btn-info btn-sm btn-flat margin-r-5 pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                </sec:authorize>

                                <sec:authorize access="hasRole('Role_Cheque_Create')">
                                    <a href="${pageContext.request.contextPath}/cheque/list" class="btn btn-primary btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span>&nbsp; List</a>
                                </sec:authorize>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <strong>ReferenceNum</strong><br>
                                            <span>123456789</span><br>

                                            <strong>Account Holder Name</strong><br>
                                            <span>Dhiraj Badu</span><br>

                                            <strong>Account Number</strong><br>
                                            <span>155001207001</span><br>

                                            <strong>ChequeNum</strong><br>
                                            <span>ch 35639576</span><br>

                                            <strong>Amount</strong><br>
                                            <span>Dollor 1000</span>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <strong>Branch</strong><br>
                                            <span>Baneshwor</span><br>

                                            <strong>Courier Date</strong><br>
                                            <span>2018-02-24</span><br>

                                            <strong>Realized Date</strong><br>
                                            <span>2018-02-24</span><br>

                                            <strong>Value Date</strong><br>
                                            <span>2018-02-24</span><br>

                                            <strong>Returned Date</strong>
                                            <span>2018-02-24</span><br>

                                            <strong>Tracking Status</strong>
                                            <span>Received By CCD</span>
                                        </div>

                                    </div>

                                    <div class="col-md-4">
                                        <div class="panel panel-color panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">CCD Remarks</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <p>
                                                    asddvskjvnsdvjsdnvsdjnvvakjs
                                                    asdvnsdjvnadkvsdvsdvSVSDvsdV
                                                    SDVsdvSDVSDVDSDVSvSDVSDVSDVS
                                                    SSCSCSCascASCAscascRGWEWGWGW
                                                        fvasaacacascascac
                                                        ascascascascacac
                                                        dhjcbscbajshcbahjascbcajc
                                                        scscaaabcjhbjchbjh
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <strong>cheque Account Number</strong><br>
                                            <span>123456789</span><br>

                                            <strong>cheque Bank</strong><br>
                                            <span>Axes pank</span><br>

                                            <strong>Created Date</strong><br>
                                            <span>2018-02-24</span><br>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="well well-sm">
                                            <strong>Alternative User</strong><br>
                                            <span>Baneshwor</span><br>

                                            <strong>Assiged To Clearing Unit</strong><br>
                                            <span>shashwot@gmail.com</span><br>

                                            <strong>Created By</strong><br>
                                            <span>sanjay@gmail.com</span><br>
                                        </div>
                                    </div>

                                    <div class="col-md-4">
                                        <div class="panel panel-color panel-primary">
                                                <div class="panel-heading">
                                                    <h3 class="panel-title">Remarks</h3>
                                                </div>
                                                <div class="panel-body">
                                                    <p>
                                                        asddvskjvnsdvjsdnvsdjnvvakjs
                                                        asdvnsdjvnadkvsdvsdvSVSDvsdV
                                                        fvsvsvdvsdvsdvsv
                                                    </p>
                                                </div>
                                            </div>
                                    </div>
                                </div>

                                <sec:authorize access="hasRole('Role_Cheque_Update')">
                                    <div class="row">

                                        <sec:authorize access="hasRole('Role_Cheque_Received_By_Ccd')">
                                            <div class="col-md-2">
                                                <a href="#" class="btn btn-success">Received</a>
                                            </div>
                                        </sec:authorize>

                                        <sec:authorize access="hasRole('Role_Cheque_Rejected_By_Ccd')">
                                            <div class="col-md-2">
                                                <button class="btn btn-danger">Reject</button>
                                            </div>
                                        </sec:authorize>

                                        <sec:authorize access="hasRole('Role_Cheque_Sent_To_Nostro')">
                                            <div class="col-md-2">
                                                <a href="#" class="btn btn-warning">Sent To Nostro</a>
                                            </div>
                                        </sec:authorize>

                                        <sec:authorize access="hasRole('Role_Cheque_Realized_From_Nostro')">
                                            <div class="col-md-2">
                                                <button class="btn btn-primary">Realized From Nostro</button>
                                            </div>
                                        </sec:authorize>

                                        <sec:authorize access="hasRole('Role_Cheque_Creadited')">
                                            <div class="col-md-2">
                                                <a href="#" class="btn btn-info">Creadited</a>
                                            </div>
                                        </sec:authorize>

                                        <div class="col-md-2">
                                            <a href="#" class="btn btn-default">Edit</a>
                                        </div>
                                    </div>
                                </sec:authorize>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <h1>Logger List</h1>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <table id="table2" class="table table-bordered table-hover table-striped">
                                                    <thead>
                                                    <tr>
                                                        <th>SN</th>
                                                        <th>Date</th>
                                                        <th>Field</th>
                                                        <th>Old Version</th>
                                                        <th>New Version</th>
                                                        <th>Created By</th>
                                                        <th>Remarks</th>
                                                    </tr>
                                                    </thead>
                                                    <tbody>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>


