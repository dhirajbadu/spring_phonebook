<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="cheque add">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Cheque Add</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_Cheque_View')">
                                        <a href="${pageContext.request.contextPath}/cheque/list" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                               <form action="${pageContext.request.contextPath}/cheque/save" method="post" modelAttribute="cheque" id="myform" novalidate="novalidate" >

                                   <%@include file="/pages/cheque/form.jsp" %>

                               </form>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>


        <div class="modal fade currencyModel" id="modal-default">

            <form action="#" id="currencyForm" novalidate="novalidate">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title">Add Currency</h4>
                        </div>
                        <div class="modal-body">

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Currency *</label>
                                        <input type="text" data-error=".currency" id="name" class="form-control" name="currency" placeholder="currency" required>

                                        <p class="form-error currency"></p>
                                    </div>
                                </div>
                            </div>

                            <div class="row">
                                <div class="col-md-12">
                                    <div class="form-group">
                                        <label class="control-label">Country *</label>
                                        <select name="currenceyInfoId" data-error=".currenceyInfoId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select currency" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                            <option >select any country</option>
                                            <c:forEach items="${countryList}" var="country">
                                                <option value="${country.countryId}">${country.countryName}</option>
                                            </c:forEach>
                                        </select><br>
                                        <p class="form-error currenceyInfoId"></p>
                                    </div>
                                </div>
                            </div>

                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-primary">Save changes</button>
                        </div>
                    </div>
                    <!-- /.modal-content -->
                </div>
                <!-- /.modal-dialog -->
            </form>
        </div>
        <!-- /.modal currency -->
    </jsp:body>

</t:genericpage>




