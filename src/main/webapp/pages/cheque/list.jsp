<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 3/10/18
  Time: 1:38 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="cheque list">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Cheque List</h3>
                                <sec:authorize access="hasRole('Role_Cheque_Create')">
                                    <a href="${pageContext.request.contextPath}/cheque/add" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                </sec:authorize>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="table2" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>ReferenceNum</th>
                                                    <th>Branch</th>
                                                    <th>Currency</th>
                                                    <th>Account Holder Name</th>
                                                    <th>Account Number</th>
                                                    <th>ChequeNum</th>
                                                    <th>Amount</th>
                                                    <th>Tracker</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <%--<c:forEach var="branch" items="${branchList}" varStatus="i">
                                                    <tr>
                                                        <td>${i.index + 1}</td>
                                                        <td>${branch.code}</td>
                                                        <td>${branch.street}</td>
                                                        <td>${branch.email}</td>
                                                        <td>${branch.contact}</td>
                                                    </tr>
                                                </c:forEach>--%>
                                                <tr>
                                                    <th>SN</th>
                                                    <th><a href="${pageContext.request.contextPath}/cheque/show">ReferenceNum</a> </th>
                                                    <th>Branch</th>
                                                    <th>Currency</th>
                                                    <th>Account Holder Name</th>
                                                    <th>Account Number</th>
                                                    <th>ChequeNum</th>
                                                    <th>Amount</th>
                                                    <th>Tracker</th>
                                                </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <c:if test="${fn:length(pagelist) gt 1}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <nav class="pull-right">
                                                <ul class="pagination">

                                                    <c:if test="${currentpage > 1}">
                                                        <li class="page-item">

                                                            <a href="${pageContext.request.contextPath}/cheque/list?pageNo=${currentpage-1}"
                                                               class="page-link">Prev</a>
                                                        </li>
                                                    </c:if>

                                                    <c:forEach var="pagelist" items="${pagelist}">
                                                        <c:choose>
                                                            <c:when test="${pagelist == currentpage}">

                                                                <li class="page-item active">
                                                          <span class="page-link">
                                                            ${pagelist}
                                                            <span class="sr-only">(current)</span>
                                                          </span>
                                                                </li>

                                                            </c:when>
                                                            <c:otherwise>

                                                                <li class="page-item"><a class="page-link"
                                                                                         href="${pageContext.request.contextPath}/cheque/list?pageNo=${pagelist}">${pagelist}</a>
                                                                </li>

                                                            </c:otherwise>

                                                        </c:choose>
                                                    </c:forEach>

                                                    <c:if test="${currentpage + 1 <= lastpage}">
                                                        <li class="page-item">
                                                            <a class="page-link"
                                                               href="${pageContext.request.contextPath}/cheque/list?pageNo=${currentpage+1}">Next</a>
                                                        </li>
                                                    </c:if>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </c:if>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

