<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Reference Number *</label>
                <input type="text" class="form-control" value="${cheque.referenceNumber}" name="referenceNumber" placeholder="Reference Number" required>
                <p class="form-error">${chequeError.referenceNumber}</p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Currencey * &nbsp; <a href="#" data-toggle="modal" data-target=".currencyModel">new currency</a></label>
                <select name="currenceyInfoId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select currency" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${currencyList}" var="currency">
                        <c:choose>
                            <c:when test="${currency.currencyId eq cheque.currenceyInfoId}">
                                <option selected="selected" value="${currency.currencyId}">${currency.currency}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${currency.currencyId}">${currency.currency}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <p class="form-error">${chequeError.currencyInfo}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Account Holder * &nbsp; <a href="#" data-toggle="modal" data-target=".accountModel">new account holder</a> </label>
                <select name="accountHolderId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select account holder" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${accountHolderList}" var="accountHolder">
                        <c:choose>
                            <c:when test="${accountHolder.accountHolderId eq cheque.accountHolderId}">
                                <option selected="selected" value="${accountHolder.accountHolderId}">${accountHolder.accountNumber}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${accountHolder.accountHolderId}">${accountHolder.accountNumber}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <p class="form-error">${chequeError.accountHolder}</p>
            </div>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Cheque Hodler Account Number *</label>
                <input type="text" class="form-control" value="${cheque.chequeHodlerAccountNumber}" name="chequeHodlerAccountNumber" placeholder="Cheque Hodler Account Number" required>
            </div>
            <p class="form-error">${chequeError.chequeHodlerAccountNumber}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Cheque Number *</label>
                <input type="text" class="form-control" value="${cheque.chequeNumber}" name="chequeNumber" placeholder="Cheque Number" required>
            </div>
            <p class="form-error">${chequeError.chequeNumber}</p>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Cheque Amount *</label>
                <input type="number" class="form-control" value="${cheque.chequeAmount}" name="chequeAmount" placeholder="Cheque Amount" required>
                <p class="form-error">${chequeError.chequeAmount}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Cheque Bank * &nbsp; <a href="#" data-toggle="modal" data-target=".bankModel" class="pull-right">new bank</a></label>
                <select name="chequeBankId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select account holder" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${bankList}" var="bank">
                        <c:choose>
                            <c:when test="${bank.bankId eq cheque.chequeBankId}">
                                <option selected="selected" value="${bank.bankId}">${bank.bankName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${bank.bankId}">${bank.bankName}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${chequeError.chequeBank}</p>
        </div>

        <div class="form-group">
            <label class="control-label">Cheque Courier Date</label>
            <div class='input-group date datepicker'>
                <input type="text" class="datepicker form-control" onkeypress="return false;" onkeyup="return false;" value="<fmt:formatDate pattern="MM/dd/yyyy" value="${cheque.chequeCourierDate}"/>" name="chequeCourierDate" placeholder="Cheque Courier Date" required>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
            </div>
            <p class="form-error">${chequeError.chequeCourierDate}</p>
        </div>

    </div>

    <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Alternative Staff *</label>
                <select name="alternativeUserId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select user" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${alternativeUserList}" var="user">
                        <c:choose>
                            <c:when test="${user.userId eq cheque.alternativeUserId}">
                                <option selected="selected" value="${user.username}">${user.userId}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${user.userId}">${user.username}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${chequeError.alternativeUser}</p>
        </div>

        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">Assigne To Clearing Unit*</label>
                <select name="assigneToClearingUnitId" class="form-control select2 select2-hidden-accessible" data-placeholder="Select user" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${clearingUnitList}" var="user">
                        <c:choose>
                            <c:when test="${user.userId eq cheque.alternativeUserId}">
                                <option selected="selected" value="${user.username}">${user.userId}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${user.userId}">${user.username}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${chequeError.assigneToClearingUnit}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Remarks *</label>
                <textarea class="form-control editor" rows="7" name="remarks" placeholder="Remarks" maxlength="300" required>${cheque.remarks}</textarea>
                <p class="form-error">${chequeError.remarks}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Upload Cheque Copy *</label>
                <input type="file" class="form-control" name="chequeCopy" required/>
                <p class="form-error">${chequeError.chequeCopy}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Upload Collection Sheet *</label>
                <input type="file" class="form-control" name="collectionSheet" required/>
                <p class="form-error">${chequeError.collectionSheet}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Upload Deposite Slip *</label>
                <input type="file" class="form-control" name="depositeSlip" required/>
                <p class="form-error">${chequeError.depositeSlip}</p>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>


<div class="modal fade accountModel" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Account Holder</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Account Number *</label>
                            <input type="text" class="form-control" name="accountNumber" placeholder="Account Number" required>

                            <p class="form-error accountNumber"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Name *</label>
                            <input type="text" class="form-control" name="holderName" placeholder="Account Holder Name" required>

                            <p class="form-error holderName"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="accountEmail" placeholder="email">

                            <p class="form-error accountEmail"></p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal account -->


<div class="modal fade bankModel" id="modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Add Bank</h4>
            </div>
            <div class="modal-body">

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Name *</label>
                            <input type="text" class="form-control" name="bankName" placeholder="currency" required>

                            <p class="form-error bankName"></p>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label class="control-label">Email</label>
                            <input type="email" class="form-control" name="bankEmail" placeholder="email">

                            <p class="form-error bankEmail"></p>
                        </div>
                    </div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save changes</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal bank -->


