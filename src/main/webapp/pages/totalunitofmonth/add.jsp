<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="t" tagdir="/WEB-INF/tags"%>

<t:genericpage title="Add Total Unit Of Month">
	<jsp:body>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">

            <!-- Main content -->
            <section class="content">

                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert"
							aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert"
							aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>

                <div class="row">
                    <div class="col-md-6">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Add Total Unit Of Month</h3>
                            </div>
                            <!-- /.box-header -->
                            <form
								action="${pageContext.request.contextPath}/totalunitofmonth/save"
								method="post" modelAttribute="totalUnitOfMonth">
                                <div class="box-body">

                                    <div class="form-group">
                                        <label class="control-label">Unit</label>
                                        <input type="text"
											class="form-control" name="unit" placeholder="Unit" required>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>

                                    <div class="form-group">
                                        <label class="control-label">Date</label>
                                        <div
											class='input-group date datepicker'>
                                            <input type="text"
												class="datepicker form-control" onkeypress="return false;"
												onkeyup="return false;"
												 name="month"
												placeholder="Date" required>
                                            <span
												class="input-group-addon">
                                        <span
												class="glyphicon glyphicon-calendar"></span>
                                    </span>
                                        </div>
                                        <p class="form-error">${fiscalYearError.opennigDate}</p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Amount</label>
                                        <input type="text"
											class="form-control" name="amount" placeholder="Amount"
											required>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>
                                    
                                    <div class="form-group">
                                        <label class="control-label">Fiscal Year</label>
                                        <select name="fiscalYearInfoId"
											class="form-control select2 select2-hidden-accessible"
											style="width: 100%;" tabindex="-1" aria-hidden="true" required>
						<c:if test="${fn:length(fiscalYearList) gt 0}">

							<option value="" selected disabled>Select Fiscal Year</option>

							<c:forEach var="fiscalyear" items="${fiscalYearList}">


								<option value="${fiscalyear.fiscalYearInfoId}">${fiscalyear.title}</option>

							</c:forEach>
						</c:if>

					</select>
                                        <p class="form-error">${fiscalYearError.title}</p>
                                    </div>



                                </div>
                                <!-- /.box-body -->
                                <div class="modal-footer">
                                    <input type="hidden"
										name="${_csrf.parameterName}" value="${_csrf.token}" />
                                    <button type="submit"
										class="btn btn-primary">Save changes</button>
                                </div>
                            </form>
                        </div>
                        <!-- /.box -->
                    </div>
                </div>

            </section>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
    </jsp:body>

</t:genericpage>


