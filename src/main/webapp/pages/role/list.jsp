<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:10 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="role list">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Role List</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_Role_Create')">
                                        <a href="${pageContext.request.contextPath}/role/add" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="table1" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Title</th>
                                                    <th>Role</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">
                                                <c:forEach var="role" items="${roleList}" varStatus="i">
                                                    <tr>
                                                        <td>${i.index + 1}</td>
                                                        <td>${role.title}</td>
                                                        <td>
                                                            <c:forEach var="permission" items="${role.permissionSet}">
                                                                <span class="badge bg-green">${permission}</span>&nbsp;
                                                            </c:forEach>
                                                        </td>
                                                        <td><a href="${pageContext.request.contextPath}/role/edit?roleId=${role.roleId}" class="btn btn-sm btn-danger"><i class="fa fa-pencil"></i>&nbsp;edit</a> </td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

