<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/19/18
  Time: 11:49 AM
  To change this template use File | Settings | File Templates.
--%>

<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>

<li class="header">HEADER</li>

<li><a href="${pageContext.request.contextPath}/dashboard"><i class="fa fa-home"></i> <span>Dashboard</span></a></li>

<sec:authorize access="hasRole('Role_User_View')">
    <li><a href="${pageContext.request.contextPath}/user/list"><i class="fa fa-user-o"></i> <span>User</span></a></li>
</sec:authorize>

<sec:authorize access="hasRole('Role_Role_View')">
    <li><a href="${pageContext.request.contextPath}/role/list"><i class="fa fa-user-secret"></i> <span>Role</span></a></li>
</sec:authorize>