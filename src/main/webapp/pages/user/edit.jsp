<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<t:genericpage title="user edit">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>

                <c:if test="${ userError ne null}">
                    <c:if test="${not empty userError.error}">
                        <div class="alert alert-danger alert-dismissable">
                            <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                            <strong>${userError.error}</strong>
                        </div>
                    </c:if>
                </c:if>

                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">User edit</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('Role_User_View')">
                                        <a href="${pageContext.request.contextPath}/user/list" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <form action="${pageContext.request.contextPath}/user/update" method="POST" modelAttribute="user" >
                                    <input type="hidden" name="userId" value="${user.userId}" />
                                    <input type="hidden" name="version" value="${user.version}" />
                                    <div class="box-body">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">User Type *</label>
                                                    <select name="userType" class="form-control select2 select2-hidden-accessible" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                                        <c:forEach var="userType" items="${userTypeList}">
                                                            <c:choose>
                                                                <c:when test="${userType eq user.userType}">
                                                                    <option selected="selected">${userType}</option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option>${userType}</option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <p class="form-error">${userError.userType}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="control-label">Role *</label>
                                                    <select name="roleIdSet" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select Permissions" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                                                        <c:forEach items="${roleList}" var="role">
                                                            <c:choose>
                                                                <c:when test="${fn:length(user.roleSet) gt 0}">
                                                                    <c:set var="containsValue" value="${false}"></c:set>
                                                                    <c:forEach items="${user.roleSet}" var="roleSet">
                                                                        <c:if test="${roleSet.roleId eq role.roleId}">
                                                                            <c:set var="containsValue" value="${true}"></c:set>
                                                                        </c:if>
                                                                    </c:forEach>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <c:choose>
                                                                        <c:when test="${fn:length(user.roleIdSet) gt 0}">
                                                                            <spring:eval var="containsValue" expression="user.roleIdSet.contains(role.roleId )" />
                                                                        </c:when>
                                                                        <c:otherwise>
                                                                            <c:set var="containsValue" value="${false}"></c:set>
                                                                        </c:otherwise>
                                                                    </c:choose>
                                                                </c:otherwise>
                                                            </c:choose>
                                                            <c:choose>
                                                                <c:when test="${containsValue eq true}">
                                                                    <option value="${role.roleId}" selected="selected">${role.title}</option>
                                                                </c:when>
                                                                <c:otherwise>
                                                                    <option value="${role.roleId}">${role.title}</option>
                                                                </c:otherwise>
                                                            </c:choose>
                                                        </c:forEach>
                                                    </select>
                                                </div>
                                                <p class="form-error">${userError.authority}</p>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.box-body -->
                                    <div class="modal-footer">
                                        <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
                                        <button type="submit" class="btn btn-primary">Save changes</button>
                                    </div>
                                </form>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>


