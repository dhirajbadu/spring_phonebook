<%--
  Created by IntelliJ IDEA.
 User: dhiraj
  Date: 2/24/18
  Time: 2:47 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="user details">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">User Details</h3>
                                <div class="box-tools">

                                    <sec:authorize access="hasRole('Role_User_Create')">
                                        <a href="${pageContext.request.contextPath}/user/add" class="btn btn-info btn-sm btn-flat margin-r-5"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                    </sec:authorize>

                                    <sec:authorize access="hasRole('Role_User_View')">
                                        <a href="${pageContext.request.contextPath}/user/list" class="btn btn-primary btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-list"></span>&nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row">
                                    <div class="col-md-12">
                                        <strong><i class="fa fa-user"></i> User Email</strong>
                                        <p class="text-muted">${user.username}</p>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <strong><i class="fa fa-user-times"></i> User Type</strong>
                                        <p class="text-muted">${user.userType}</p>
                                        <hr>
                                    </div>
                                </div>

                                <c:if test="${user.branchDTO ne null}">
                                    <div class="row">
                                        <div class="col-md-12">
                                            <strong><i class="fa fa-tags"></i> Branch</strong>
                                            <p class="text-muted">${user.branchDTO.code}</p>
                                            <hr>
                                        </div>
                                    </div>
                                </c:if>

                                <div class="row">
                                    <div class="col-md-12">
                                        <strong><i class="fa fa-user-secret"></i> Role</strong>
                                        <table class="table table-hover table-striped">
                                            <tbody>
                                            <c:forEach items="${user.roleSet}" var="role">
                                                <tr>
                                                    <td>${role.title}</td>
                                                    <td>
                                                        <c:forEach var="permission" items="${role.permissionSet}">
                                                            <span class="badge bg-green">${permission}</span>
                                                        </c:forEach>
                                                    </td>
                                                </tr>
                                            </c:forEach>
                                            </tbody>
                                        </table>
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <strong><span class="glyphicon glyphicon-lock"></span> Status</strong>
                                        <p>
                                            <c:choose>
                                                <c:when test="${user.enabled}">
                                                    <span class="label label-success">Active</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="label label-danger">Inactive</span>
                                                </c:otherwise>
                                            </c:choose>

                                            <c:choose>
                                                <c:when test="${user.status eq 'ACTIVE'}">
                                                    <span class="label label-success">Unlocked</span>
                                                </c:when>
                                                <c:otherwise>
                                                    <span class="label label-danger">Locked</span>
                                                </c:otherwise>
                                            </c:choose>
                                        </p>
                                        <hr>
                                    </div>
                                </div>

                                <sec:authorize access="hasRole('Role_User_Update')">
                                    <div class="row">
                                    <div class="col-md-12 well well-sm">
                                        <c:choose>
                                            <c:when test="${user.enabled}">
                                                <a href="${pageContext.request.contextPath}/user/enable?userId=${user.userId}&enable=false" class="btn btn-danger"><i class="fa fa-close"></i>&nbsp;Inactivate</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${pageContext.request.contextPath}/user/enable?userId=${user.userId}&enable=true" class="btn btn-success"><i class="fa fa-stop-circle-o"></i>&nbsp;Activate</a>
                                            </c:otherwise>
                                        </c:choose>

                                        <c:choose>
                                            <c:when test="${user.status eq 'ACTIVE'}">
                                                <a href="${pageContext.request.contextPath}/user/lock?userId=${user.userId}&lock=true" class="btn btn-danger"><i class="fa fa-lock"></i>&nbsp;Lock</a>
                                            </c:when>
                                            <c:otherwise>
                                                <a href="${pageContext.request.contextPath}/user/lock?userId=${user.userId}&lock=false" class="btn btn-success"><i class="fa fa-unlock"></i>&nbsp;Unlock</a>
                                            </c:otherwise>
                                        </c:choose>

                                        <a href="${pageContext.request.contextPath}/user/edit?userId=${user.userId}" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-pencil"></span> &nbsp;Edit</a>
                                    </div>
                                </div>
                                </sec:authorize>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>
