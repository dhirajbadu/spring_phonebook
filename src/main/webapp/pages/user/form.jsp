<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 9:43 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <label class="control-label">Email *</label>
                <input class="form-control" name="username" value="${user.username}" placeholder="email" type="email" required>
                <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
            </div>
            <p class="form-error">${userError.username}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <label class="control-label">Password *</label>
                <input class="form-control" name="password" placeholder="password" type="password" required>
                <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <p class="form-error">${userError.password}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group has-feedback">
                <label class="control-label">Confirm Password *</label>
                <input class="form-control" name="repassword" placeholder="retype password" type="password" required>
                <span class="glyphicon glyphicon-log-in form-control-feedback"></span>
            </div>
            <p class="form-error">${userError.repassword}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">User Type *</label>
                <select name="userType" id="userType" class="form-control select2 select2-hidden-accessible" data-placeholder="Select User Type" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach var="userType" items="${userTypeList}">
                        <c:choose>
                            <c:when test="${userType eq user.userType}">
                                <option selected="selected">${userType}</option>
                            </c:when>
                            <c:otherwise>
                                <option>${userType}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${userError.userType}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Role *</label>
                <select name="roleIdSet" class="form-control select2 select2-hidden-accessible" multiple="" data-placeholder="Select Permissions" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${roleList}" var="role">
                        <c:choose>
                            <c:when test="${fn:length(user.roleIdSet) gt 0}">
                                <spring:eval var="containsValue" expression="user.roleIdSet.contains(role.roleId )" />
                            </c:when>
                            <c:otherwise>
                                <c:set var="containsValue" value="${false}"></c:set>
                            </c:otherwise>
                        </c:choose>
                        <c:choose>
                            <c:when test="${containsValue eq true}">
                                <option value="${role.roleId}" selected="selected">${role.title}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${role.roleId}">${role.title}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${userError.authority}</p>
        </div>
    </div>


</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

