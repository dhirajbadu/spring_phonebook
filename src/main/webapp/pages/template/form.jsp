<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${template.title}" name="title" placeholder="title" required>
                <p class="form-error">${templateError.code}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Type *</label>
                <select name="templateType" class="form-control select2 select2-hidden-accessible" data-placeholder="Select type" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${templateTypeList}" var="templateType">
                           <c:choose>
                                <c:when test="${templateType eq template.templateType}">
                                    <option selected="selected" value="${templateType}">${templateType}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${templateType}">${templateType}</option>
                                </c:otherwise>
                            </c:choose>
                    </c:forEach>
                </select>
            </div>
            <p class="form-error">${templateError.city}</p>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">isRequired *</label>
                <input type="checkbox" class="btn btn-lg btn-default" value="${template.required}" name="required" >
                <p class="form-error">${templateError.required}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">MaxLendth *</label>
                <input type="text" class="form-control" value="${template.maxLendth}" name="maxLendth" placeholder="maxLendth" required>
                <p class="form-error">${templateError.maxLendth}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">MinLendth *</label>
                <input type="text" class="form-control" value="${template.minLendth}" name="minLendth" placeholder="minLendth" required>
                <p class="form-error">${templateError.minLendth}</p>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

