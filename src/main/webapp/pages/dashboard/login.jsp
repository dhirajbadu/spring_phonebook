<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<%--fav icon start--%>
	<link rel="apple-touch-icon" sizes="57x57" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="${pageContext.request.contextPath}/resources/images/fav/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192"  href="${pageContext.request.contextPath}/resources/images/fav/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="${pageContext.request.contextPath}/resources/images/fav/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="${pageContext.request.contextPath}/resources/images/fav/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="${pageContext.request.contextPath}/resources/images/fav/favicon-16x16.png">
	<link rel="manifest" href="${pageContext.request.contextPath}/resources/images/fav/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="${pageContext.request.contextPath}/resources/images/fav/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">
	<%--fav icon end--%>
<title>Log in</title>
<!-- Tell the browser to be responsive to screen width -->
<meta
	content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no"
	name="viewport">
<!-- Bootstrap 3.3.7 -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<!-- Font Awesome -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/font-awesome.min.css">
<!-- Ionicons -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/ionicons.min.css">
<!-- Theme style -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/css/AdminLTE.min.css">
<!-- iCheck -->
<link rel="stylesheet"
	href="${pageContext.request.contextPath}/resources/plugins/iCheck/square/blue.css">

<!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
<!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

	<script src='https://www.google.com/recaptcha/api.js'></script>
<!-- Google Font -->
<link rel="stylesheet"
	href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">
	<div class="login-box">
		<div class="login-logo">
			<a href="${pageContext.request.contextPath}/"><b>PhoneBook</b><br>Management<br/>
				System
			</a>
		</div>
		<!-- /.login-logo -->
		<div class="login-box-body">
			<p class="login-box-msg">Sign in to start your session</p>
			<c:if test="${error ne null}">
				<label class="has-error" style="color: red;">${error}</label>
			</c:if>
			<c:if test="${message ne null}">
				<label class="has-error" style="color: green;">${message}</label>
			</c:if>
			<form action="${pageContext.request.contextPath}/authlogin"
				method="post">
				<div class="form-group has-feedback">
					<input type="text" class="form-control" placeholder="Username"
						name="j_username" autofocus> <span
						class="glyphicon glyphicon-user form-control-feedback"></span>
				</div>
				<div class="form-group has-feedback">
					<input type="password" class="form-control" placeholder="Password"
						name="j_password"> <span
						class="glyphicon glyphicon-lock form-control-feedback"></span>
				</div>
				<div class="row">
					<!-- /.col -->
					<div class="col-xs-4">
						<input type="hidden" name="${_csrf.parameterName}"
							value="${_csrf.token}" />
						<button type="submit" class="btn btn-primary btn-block btn-flat">Sign
							In</button>
					</div>

					<!-- /.col -->
				</div>
				<hr />
				<div class="row">
					<div class="col-xs-12 float-right">

						<span class="recoverPass" style="cursor: pointer;">Forget
							Password!</span>
					</div>
				</div>
			</form>
			<div class="forgetpassarea" style="display: none;">
				<form action="${pageContext.request.contextPath}/user/forgetpass"
					method="post" id="registerForm">
					<div class="row">
						<div class="form-group has-feedback">
							<input type="text" class="form-control" placeholder="Username"
								   name="username" autofocus> <span
								class="glyphicon glyphicon-user form-control-feedback"></span>
						</div>
					</div>

					<div class="row">
						<div class="col-md-4 form-group">
							<div class="g-recaptcha" data-callback="capcha_filled" data-expired-callback="capcha_expired" data-sitekey="6LeG-kMUAAAAACApsSOmYiulgmDIMHn6aMqaKeUM"></div>
						</div>
					</div>

					<div class="row">
						<!-- /.col -->
						<div class="col-xs-4">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" />
							<button type="submit" class="btn btn-primary btn-block btn-flat">Submit</button>
						</div>

						<!-- /.col -->
					</div>

				</form>
			</div>

			<%--<a href="#">I forgot my password</a><br>
        <a href="#" class="text-center">Register a new member</a>--%>

		</div>
		<!-- /.login-box-body -->
	</div>
	<!-- /.login-box -->

	<!-- jQuery 3 -->
	<script
		src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
	<!-- Bootstrap 3.3.7 -->
	<script
		src="${pageContext.request.contextPath}/resources/js/bootstrap.min.js"></script>
	<!-- iCheck -->
	<script
		src="${pageContext.request.contextPath}/resources/plugins/iCheck/icheck.min.js"></script>
	<script>
		$(function() {
			$('input').iCheck({
				checkboxClass : 'icheckbox_square-blue',
				radioClass : 'iradio_square-blue',
				increaseArea : '20%' // optional
			});
		});

		$(document).ready(function() {
			
			$('.recoverPass').click(function() {
				$('.forgetpassarea').toggle();
			});
		});
	</script>


	<script>
        var allowSubmit = false;

        var msg = "proof you are not a robot\n click on recapcha";

        function capcha_filled () {
            allowSubmit = true;
        }

        function capcha_expired () {
            allowSubmit = false;
            msg = "captcha is expired , click again on I'm not a robot"
        }

        function check_if_capcha_is_filled (e) {
            if(allowSubmit) return true;
            e.preventDefault();
            swal(msg);
        }

        $(document).ready(function () {

            $("#registerForm").submit(function(e){
                check_if_capcha_is_filled (e);
            });
        })
	</script>

</body>
</html>
