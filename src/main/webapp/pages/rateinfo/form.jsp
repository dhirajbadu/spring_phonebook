<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>

<div class="box-body">

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Title *</label>
                <input type="text" class="form-control" value="${rateInfo.title}" name="title" placeholder="Title" required>
                <p class="form-error">${rateInfoError.title}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Code *</label>
                <input type="text" class="form-control" value="${rateInfo.code}" name="code" placeholder="Code" required>
                <p class="form-error">${rateInfoError.code}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Client Type *</label>
                <select name="clientTypeInfoId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select City" style="width: 100%;" tabindex="-1" aria-hidden="true" url="${pageContext.request.contextPath}/ajax/rateinfo/get/last" required>
                    <option value="0">select client type</option>
                    <c:forEach items="${clientTypeList}" var="clientType">
                        <c:choose>
                            <c:when test="${clientType.clientTypeInfoId eq rateInfo.clientTypeInfoId}">
                                <option selected="selected" value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <p class="form-error">${rateInfoError.clientTypeInfoId}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Unit From *</label>
                <input type="number" id="unitFrom" class="form-control" value="${rateInfo.unitFrom}" name="unitFrom" placeholder="Unit From" disabled="disabled" required>
                <%--<p class="form-error">${rateInfoError.unitFrom}</p>--%>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Unit To *</label>
                <input type="number" class="form-control" value="${rateInfo.unitTo}" name="unitTo" placeholder="Unit To" required>
                <p class="form-error">${rateInfoError.unitTo}</p>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <div class="form-group">
                <label class="control-label">Rate *</label>
                <input type="number" class="form-control" value="${rateInfo.rate}" name="rate" placeholder="Rate" required>
                <p class="form-error">${rateInfoError.rate}</p>
            </div>
        </div>
    </div>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Save changes</button>
</div>

