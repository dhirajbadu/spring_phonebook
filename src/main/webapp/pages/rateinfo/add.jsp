<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 2/21/18
  Time: 11:30 PM
  To change this template use File | Settings | File Templates.
--%>

<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="RateInfo Add">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">RateInfo Add</h3>
                                <div class="box-tools">
                                    <sec:authorize access="hasRole('System')">
                                        <a href="${pageContext.request.contextPath}/rateinfo/list"
                                           class="btn btn-info btn-sm btn-flat pull-right"><span
                                                class="glyphicon glyphicon-list"></span> &nbsp; List</a>
                                    </sec:authorize>
                                </div>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <form action="${pageContext.request.contextPath}/rateinfo/save" method="post"
                                      modelAttribute="rateInfo">

                                    <%@include file="/pages/rateinfo/form.jsp" %>

                                </form>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

<script>
    var userRequest = null;
    $(document).ready(function () {
        $('select').on('change', function() {
            clientTypeId($('select').attr("url") , this.value)
        });
    });

    function clientTypeId(url , clientTypeId) {

        userRequest = $.ajax({
            type: "GET",
            url: url,
            contentType: "application/x-www-form-urlencoded;charset=utf-8",
            data: {clientTypeId: clientTypeId},
            dataType: 'json',
            timeout: 30000,
            tryCount: 0,
            retryLimit: 3,
            beforeSend: function () {
                if (userRequest !== undefined) {
                    if (userRequest !== null) {

                        userRequest.abort();
                    }
                }
            },
            success: function (data) {

                var result = data.detail;

                var msg = data.message;

                if (data.status === 'SUCCESS') {

                    console.log("success");
                    if(result === null){
                        console.log("null");
                        $("#unitFrom").prop("disabled", true);
                        $("#unitFrom").val("0");
                    }else {
                        $("#unitFrom").val("");
                        $("#unitFrom").val(result.unitTo + 1);
                        $("#unitFrom").prop("disabled", true);
                        console.log("value : " + (result.unitTo + 1));
                    }
                }

                if (data.status === 'VALIDATION_FAILED') {
                   alert(msg);
                }
            },
            error: function (xhr, textStatus, errorThrown) {

                console.log(xhr + " " + textStatus + " " + errorThrown);

                if (textStatus === 'timeout') {

                    this.tryCount++;

                    if (this.tryCount <= this.retryLimit) {
                        //try again
                        $.ajax(this);
                        return;
                    } else {
                        //cancel request
                        stopLoading(sppiner);
                        window.location.reload();

                        return;
                    }

                }

                if (xhr.status === 500) {
                    //handle error
                    window.location.reload();
                } else {
                    //handle error
                    window.location.reload();
                }
            }
        });

    }
</script>
