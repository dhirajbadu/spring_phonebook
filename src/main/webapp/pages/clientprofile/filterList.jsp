<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="Client Application Filter">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Client Application Filter</h3>
                                <sec:authorize access="hasRole('Role_ClientProfileInfo_View')">
                                    <a href="${pageContext.request.contextPath}/clientapplication/list"
                                       class="btn btn-info btn-sm btn-flat pull-right"><span
                                            class="glyphicon glyphicon-plus-list"></span>&nbsp; List</a>
                                </sec:authorize>
                                    <%--  <div class="box-tools">
                                          <button class="btn btn-success btn-sm btn-flat pull-right filter" data-toggle="collapse" data-target="#filter"><span class="glyphicon glyphicon-filter"></span> filter</button>
                                      </div>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row" id="filter">
                                    <div class="col-md-12">
                                        <%@include file="/pages/clientprofile/filterForm.jsp" %>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Email</th>
                                                    <th>Street</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">

                                                <c:forEach varStatus="i" var="client" items="${clientInfoList}">
                                                    <tr>
                                                        <td>${i.index + 1}</td>
                                                        <td><a href="${pageContext.request.contextPath}/clientapplication/show/${client.clientProfileInfoId}">${client.applicantName}</a></td>
                                                        <td>${client.applicantContact}</td>
                                                        <td>${client.applicantEmail}</td>
                                                        <td>${client.street}</td>
                                                    </tr>
                                                </c:forEach>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>


                                <c:if test="${fn:length(pagelist) gt 1}">

                                    <div class="col-xs-12">
                                        <nav class="pull-right">
                                            <ul class="pagination">

                                                <c:if test="${currentpage > 1}">
                                                    <li class="page-item">

                                                        <a href="${pageContext.request.contextPath}/clientapplication/filter?pageNo=${currentpage-1}&clientName=${filterDTO.clientName}&wardInfoId=${filterDTO.wardInfoId}&blockInfoId=${filterDTO.blockInfoId}"
                                                           class="page-link">Prev</a>
                                                    </li>
                                                </c:if>

                                                <c:forEach var="pagelist" items="${pagelist}">
                                                    <c:choose>
                                                        <c:when test="${pagelist == currentpage}">

                                                            <li class="page-item active">
                                                  <span class="page-link">
                                                    ${pagelist}
                                                    <span class="sr-only">(current)</span>
                                                  </span>
                                                            </li>

                                                        </c:when>
                                                        <c:otherwise>

                                                            <li class="page-item"><a class="page-link" href="${pageContext.request.contextPath}/clientapplication/filter?pageNo=${pagelist}&clientName=${filterDTO.clientName}&wardInfoId=${filterDTO.wardInfoId}&blockInfoId=${filterDTO.blockInfoId}">${pagelist}</a>
                                                            </li>

                                                        </c:otherwise>

                                                    </c:choose>
                                                </c:forEach>

                                                <c:if test="${currentpage + 1 <= lastpage}">
                                                    <li class="page-item">
                                                        <a class="page-link"
                                                           href="${pageContext.request.contextPath}/clientapplication/filter?pageNo=${currentpage+1}&clientName=${filterDTO.clientName}&wardInfoId=${filterDTO.wardInfoId}&blockInfoId=${filterDTO.blockInfoId}">Next</a>
                                                    </li>
                                                </c:if>
                                            </ul>
                                        </nav>
                                    </div>

                                </c:if>


                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

<script>
    $(document).ready(function () {


    });
</script>