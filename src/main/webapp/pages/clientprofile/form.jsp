<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/22/18
  Time: 8:34 PM
  To change this template use File | Settings | File Templates.
--%>

<div class="box-body">

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Applicant Name *</label>
                <input type="text" class="form-control" value="${clientProfileInfo.applicantName}" name="applicantName"
                       placeholder="Name of Applicant" required>
                <p class="form-error">${clientProfileInfoError.applicantName}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Applicant Contact *</label>
                <input type="text" class="form-control" value="${clientProfileInfo.applicantContact}"
                       name="applicantContact"
                       placeholder="Contact Number" required>
                <p class="form-error">${clientProfileInfoError.applicantContact}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Email Address</label>
                <input type="email" class="form-control" value="${clientProfileInfo.applicantEmail}"
                       name="applicantEmail" placeholder="Email address"
                       >
                <p class="form-error">${clientProfileInfoError.applicantEmail}</p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">House Number </label>
                <input type="text" class="form-control" value="${clientProfileInfo.house_no}" name="house_no"
                       placeholder="House Number" >
                <p class="form-error">${clientProfileInfoError.house_no}</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Street </label>
                <input type="text" class="form-control" value="${clientProfileInfo.street}" name="street"
                       placeholder="Street" >
                <p class="form-error">${clientProfileInfoError.street}</p>
            </div>
        </div>


        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Ward No. *</label>

                <select name="wardInfoId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select ward" style="width: 100%;" tabindex="-1" aria-hidden="true"
                        required>
                    <c:forEach items="${wardList}" var="ward">
                        <c:choose>
                            <c:when test="${ward.wardInfoId eq clientProfileInfo.wardInfoId}">
                                <option selected="selected" value="${ward.wardInfoId}">${ward.code}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${ward.wardInfoId}">${ward.code}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>

                <p class="form-error">${clientProfileInfoError.wardNo}</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Block. *</label>

                <select name="blockInfoId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select block" style="width: 100%;" tabindex="-1" aria-hidden="true"
                        required>
                    <c:forEach items="${blockList}" var="block">
                        <c:choose>
                            <c:when test="${block.blockInfoId eq clientProfileInfo.blockInfoId}">
                                <option selected="selected" value="${block.blockInfoId}">${block.code}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${block.blockInfoId}">${block.code}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>

                <p class="form-error">${clientProfileInfoError.blockInfo}</p>
            </div>
        </div>
</div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">ClientType *</label>
                <select name="clientTypeId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select ClientType" style="width: 100%;" tabindex="-1" aria-hidden="true"
                        required>
                    <c:forEach items="${clientTypeList}" var="clientType">
                        <c:choose>
                            <c:when test="${clientType.clientTypeInfoId eq clientProfileInfo.clientTypeId}">
                                <option selected="selected"
                                        value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${clientType.clientTypeInfoId}">${clientType.title}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <p class="form-error">${clientProfileInfoError.clientTypeId}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">City *</label>
                <select name="cityInfoId" class="form-control select2 select2-hidden-accessible"
                        data-placeholder="Select City" style="width: 100%;" tabindex="-1" aria-hidden="true" required>
                    <c:forEach items="${cityList}" var="city">
                        <c:choose>
                            <c:when test="${city.cityId eq clientProfileInfo.cityInfoId}">
                                <option selected="selected" value="${city.cityId}">${city.cityName}</option>
                            </c:when>
                            <c:otherwise>
                                <option value="${city.cityId}">${city.cityName}</option>
                            </c:otherwise>
                        </c:choose>
                    </c:forEach>
                </select>
                <p class="form-error">${clientProfileInfoError.cityInfoId}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Spouse Name </label>
                <input type="text" class="form-control" value="${clientProfileInfo.spouseName}" name="spouseName"
                       placeholder="Spouse Name"
                       >
                <p class="form-error">${clientProfileInfoError.spouseName}</p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Father Name </label>
                <input type="text" class="form-control" value="${clientProfileInfo.fatherName}" name="fatherName"
                       placeholder="Father's Name"
                       >
                <p class="form-error">${clientProfileInfoError.fatherName}</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Grand Father Name </label>
                <input type="text" class="form-control" value="${clientProfileInfo.grandFatherName}" name="grandFatherName"
                       placeholder="Grand Father's Name"
                >
                <p class="form-error">${clientProfileInfoError.grandFatherName}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">No. of Male *</label>
                <input type="number" class="form-control" value="${clientProfileInfo.noOfMale}" name="noOfMale"
                       placeholder="Number of Male"
                       required>
                <p class="form-error">${clientProfileInfoError.noOfMale}</p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">No. of Female *</label>
                <input type="number" class="form-control" value="${clientProfileInfo.noOfFemale}" name="noOfFemale"
                       placeholder="No. of Female"
                       required>
                <p class="form-error">${clientProfileInfoError.noOfFemale}</p>
            </div>
        </div>
        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Land Lord's Name </label>
                <input type="text" class="form-control" value="${clientProfileInfo.landLordName}" name="landLordName"
                       placeholder="Land Lord's Name"
                       >
                <p class="form-error">${clientProfileInfoError.landLordName}</p>
            </div>
        </div>

        <div class="col-md-4">
            <div class="form-group">
                <label class="control-label">Land Lord's Address </label>
                <input type="text" class="form-control" value="${clientProfileInfo.landLordAddress}" name="landLordAddress"
                       placeholder="Land Lord Address"
                       >
                <p class="form-error">${clientProfileInfoError.landLordAddress}</p>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Land Kitta No. </label>
                <input type="text" class="form-control" value="${clientProfileInfo.landKittaNo}" name="landKittaNo"
                       placeholder="Land Kitta No."
                       >
                <p class="form-error">${clientProfileInfoError.landKittaNo}</p>
            </div>
        </div>
        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Land Naksa No. </label>
                <input type="text" class="form-control" value="${clientProfileInfo.landNaksaNo}" name="landNaksaNo"
                       placeholder="Land Naksa No."
                       >
                <p class="form-error">${clientProfileInfoError.landNaksaNo}</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Land Volume </label>
                <input type="text" class="form-control" value="${clientProfileInfo.landVolume}" name="landVolume"
                       placeholder="Land Volume"
                       >
                <p class="form-error">${clientProfileInfoError.landVolume}</p>
            </div>
        </div>

        <div class="col-md-3">
            <div class="form-group">
                <label class="control-label">Land Volume Unit</label>
                <input type="text" class="form-control" value="${clientProfileInfo.landVolumeUnit}" name="landVolumeUnit"
                       placeholder="Land Volume Unit"
                >
                <p class="form-error">${clientProfileInfoError.landVolumeUnit}</p>
            </div>
        </div>

    </div>

 <%--   <div class="row">
        <div class="col-md-6">
            <div class="form-group">
                <label class="control-label">choose photo (citizenship or any )</label>
                <input type="file" class="form-control" name="doc">
                <p class="form-error">${clientProfileInfoError.doc}</p>
            </div>
        </div>
    </div>--%>

</div>
<!-- /.box-body -->
<div class="modal-footer">
    <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
    <button type="submit" class="btn btn-primary">Submit</button>
</div>

