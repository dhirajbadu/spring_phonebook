<%--
  Created by IntelliJ IDEA.
  User: bidhee
  Date: 2/16/18
  Time: 4:38 PM
  To change this template use File | Settings | File Templates.
--%>
<form action="${pageContext.request.contextPath}/clientapplication/filter" method="GET" >
    <div class="well well-sm">

        <div class="row">
            <div class="col-md-3">
                <div class="form-group">
                    <label>Client Name</label>
                    <input class="form-control" name="clientName" value="${filterDTO.clientName}" placeholder="client name"/>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Ward</label>
                    <select name="wardInfoId" class="form-control select2 select2-hidden-accessible"
                            data-placeholder="Select ward" style="width: 100%;" tabindex="-1" aria-hidden="true"
                    >
                        <option value="0">select any ward</option>
                        <c:forEach items="${wardList}" var="ward">
                            <c:choose>
                                <c:when test="${ward.wardInfoId eq filterDTO.wardInfoId}">
                                    <option selected="selected" value="${ward.wardInfoId}">${ward.code}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${ward.wardInfoId}">${ward.code}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>


                    </select>
                </div>
            </div>

            <div class="col-md-3">
                <div class="form-group">
                    <label>Block</label>
                    <select name="blockInfoId" class="form-control select2 select2-hidden-accessible"
                            data-placeholder="Select block" style="width: 100%;" tabindex="-1" aria-hidden="true"
                    >
                        <option value="0">select any block</option>
                        <c:forEach items="${blockList}" var="block">
                            <c:choose>
                                <c:when test="${block.blockInfoId eq filterDTO.blockInfoId}">
                                    <option selected="selected" value="${block.blockInfoId}">${block.code}</option>
                                </c:when>
                                <c:otherwise>
                                    <option value="${block.blockInfoId}">${block.code}</option>
                                </c:otherwise>
                            </c:choose>
                        </c:forEach>
                    </select>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="col-md-4 pull-right">
                <button type="submit" class="btn btn-success btn-flat btn-block">Filter!</button>
            </div>
        </div>

    </div>
</form>
