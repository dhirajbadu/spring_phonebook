<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="Client Application List">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Client Application</h3>
                                <sec:authorize access="hasRole('Role_ClientProfileInfo_Create')">
                                    <a href="${pageContext.request.contextPath}/clientapplication/add" class="btn btn-info btn-sm btn-flat pull-right"><span class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                </sec:authorize>
                                <%--<div class="box-tools">
                                    <button class="btn btn-success btn-sm btn-flat pull-right margin-r-5 filter" data-toggle="collapse" data-target="#filter"><span class="glyphicon glyphicon-filter"></span> filter</button>
                                </div>--%>
                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <div class="row" id="filter" >
                                    <div class="col-md-12">
                                        <%@include file="/pages/clientprofile/filterForm.jsp" %>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="">
                                            <table id="ajaxDataTable" url="${pageContext.request.contextPath}/ajax/clientprofile/list" class="table table-bordered table-hover table-striped">
                                                <thead>
                                                <tr>
                                                    <th>SN</th>
                                                    <th>Name</th>
                                                    <th>Contact</th>
                                                    <th>Email</th>
                                                    <th>Street</th>
                                                </tr>
                                                </thead>
                                                <tbody id="myData">

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>
        </div>
    </jsp:body>

</t:genericpage>

<script>
    $(document).ready(function () {

        $(function () {
            $.fn.dataTable.ext.errMode = 'none';

            $('#ajaxDataTable').on( 'error.dt', function ( e, settings, techNote, message ) {
                console.log( 'An error has been reported by DataTables: ', message );
            } ).DataTable({
                processing: true,
                serverSide: true,
                searching: true,
                ordering: true,
                lengthChange: true,
                ajax: $('#ajaxDataTable').attr('url'),
                columnDefs: [ {
                    targets: 1,
                    render: function ( data, type, row ) {
                        var result = "<a href=\"${pageContext.request.contextPath}/clientapplication/show/"+row[0]+"\" >" + row[1] +"</a>";
                        return result
                    }
                }]
            });
        });

    });
</script>