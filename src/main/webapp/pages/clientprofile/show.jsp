<%--
  Created by IntelliJ IDEA.
  User: dhiraj
  Date: 1/25/18
  Time: 7:30 AM
  To change this template use File | Settings | File Templates.
--%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib prefix="t" tagdir="/WEB-INF/tags" %>

<t:genericpage title="${clientProfileInfo.applicantName}">
    <jsp:body>
        <div class="content-wrapper">

            <section class="content">
                <c:if test="${not empty message}">
                    <div class="alert alert-success alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${message}</strong>
                    </div>
                </c:if>

                <c:if test="${not empty error}">
                    <div class="alert alert-danger alert-dismissable">
                        <a href="#" class="close" data-dismiss="alert" aria-label="close">x</a>
                        <strong>${error}</strong>
                    </div>
                </c:if>

                <div class="basePrint" style="display:none;">


                    <div class="wrapper">
                        <div class="outer">
                            <div class="content animated fadeInLeft">
                                <span class="bg animated fadeInDown">Client Info</span>
                                <h1 style="font-size:24px;"><b>Tikapur Sana Sahakari Khanepani Limited </b></h1>
                                <p>

                                <table class="table table-bordered">
                                    <tbody>

                                    <tr>
                                        <td>CID:</td>
                                        <td>${clientProfileInfo.clientProfileInfoId}</td>
                                    </tr>

                                    <tr>
                                        <td>Name:</td>
                                        <td>${clientProfileInfo.applicantName}</td>
                                    </tr>

                                    <tr>
                                        <td>Contact:</td>
                                        <td>${clientProfileInfo.applicantContact}</td>
                                    </tr>


                                    <tr>
                                        <td>House Number:</td>
                                        <td>${clientProfileInfo.house_no}</td>
                                    </tr>

                                    <tr>
                                        <td>Street:</td>
                                        <td>${clientProfileInfo.street}</td>
                                    </tr>

                                    <tr>
                                        <td>Ward No. :</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${clientProfileInfo.wardInfoDTO ne null}">
                                                    ${clientProfileInfo.wardInfoDTO.code}
                                                </c:when>
                                                <c:otherwise>
                                                    NA
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>Block:</td>
                                        <td>
                                            <c:choose>
                                                <c:when test="${clientProfileInfo.blockInfoDTO ne null}">
                                                    ${clientProfileInfo.blockInfoDTO.code}
                                                </c:when>
                                                <c:otherwise>
                                                    NA
                                                </c:otherwise>
                                            </c:choose>
                                        </td>
                                    </tr>


                                    <tr>
                                        <td>City:</td>
                                        <td>${clientProfileInfo.cityName}</td>
                                    </tr>


                                    </tbody>
                                </table>

                                </p>


                            </div>

                            <c:choose>
                                <c:when test="${clientProfileInfo.photoName ne null}">
                                    <img src="${pageContext.request.contextPath}/image/${clientProfileInfo.photoName}"
                                         style="margin-top:25%; border: 1px solid; margin-right:5%; " width="200px"
                                         class="animated fadeInRight">
                                </c:when>
                                <c:otherwise>
                                    <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg"
                                         style="margin-top:25%; border: 1px solid; margin-right:5%; " width="200px"
                                         class="animated fadeInRight">
                                </c:otherwise>
                            </c:choose>

                        </div>

                    </div>


                </div>
                <div class="row">

                    <div class="col-xs-12">
                        <div class="box box-info">
                            <div class="box-header">
                                <h3 class="box-title">Client Details</h3>
                                <c:if test="${clientProfileInfo.profileStatus.value eq 'Approved'}">
                                    <p class="text-center text-bold"><span class="label label-success">Approved</span></p>
                                </c:if>
                                <sec:authorize access="hasRole('Role_ClientProfileInfo_Create')">
                                    <a href="${pageContext.request.contextPath}/clientapplication/add"
                                       class="btn btn-info btn-sm btn-flat pull-right"><span
                                            class="glyphicon glyphicon-plus-sign"></span>&nbsp; Add</a>
                                </sec:authorize>

                                <sec:authorize access="hasRole('Role_ClientProfileInfo_View')">
                                    <a href="${pageContext.request.contextPath}/clientapplication/list"
                                       class="btn btn-primary btn-sm btn-flat pull-right"><span
                                            class="glyphicon glyphicon-list-alt"></span>&nbsp; List</a>
                                </sec:authorize>

                                <sec:authorize access="hasRole('Role_ClientProfileInfo_Update')">
                                    <a href="${pageContext.request.contextPath}/clientapplication/edit/${clientProfileInfo.clientProfileInfoId}"
                                       class="btn btn-warning btn-sm btn-flat pull-right confirmDelete" data-type="application">
                                        <span class="glyphicon glyphicon-pencil"></span>&nbsp;edit</a>
                                </sec:authorize>

                                <sec:authorize access="hasRole('Role_ClientProfileInfo_View')">
                                    <a class="btn btn-info btn-sm btn-flat pull-right printclick"><span
                                            class="glyphicon glyphicon-print"></span>&nbsp; Print</a>
                                </sec:authorize>

                            </div>
                            <!-- /.box-header -->
                            <div class="box-body">

                                <c:choose>
                                    <c:when test="${clientProfileInfo ne null}">

                                        <div class="row">
                                            <div class="col-md-6">
                                                <p>
                                                    <c:choose>
                                                        <c:when test="${clientProfileInfo.photoName ne null}">
                                                            <a href="${pageContext.request.contextPath}/image/${clientProfileInfo.photoName}"
                                                               target="_blank"><img
                                                                    src="${pageContext.request.contextPath}/image/${clientProfileInfo.photoName}"
                                                                    style="width:200px"></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg"
                                                                 style="width:200px">
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>
                                            <div class="col-md-4 pull-right">
                                                <p class="well well-sm"><label>Registration Date :</label> <i
                                                        class="fa fa-ca"></i>&nbsp;<label><fmt:formatDate
                                                        pattern="yyyy-MM-dd"
                                                        value="${clientProfileInfo.createdDate}"/></label></p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Applicant Name</label>
                                                <p class="well well-sm">${clientProfileInfo.applicantName}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Applicant Contact</label>
                                                <p class="well well-sm">${clientProfileInfo.applicantContact}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Email Address</label>
                                                <p class="well well-sm">${clientProfileInfo.applicantEmail}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>House Number</label>
                                                <p class="well well-sm">${clientProfileInfo.house_no}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Street</label>
                                                <p class="well well-sm">${clientProfileInfo.street}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Ward No.</label>
                                                <p class="well well-sm">
                                                    <c:choose>
                                                        <c:when test="${clientProfileInfo.wardInfoDTO ne null}">
                                                            ${ clientProfileInfo.wardInfoDTO.code}
                                                        </c:when>
                                                        <c:otherwise>
                                                            NA
                                                        </c:otherwise>
                                                    </c:choose>

                                                </p>
                                            </div>

                                            <div class="col-md-3">
                                                <label>Block</label>
                                                <p class="well well-sm">
                                                    <c:choose>
                                                        <c:when test="${clientProfileInfo.blockInfoDTO ne null}">
                                                            ${ clientProfileInfo.blockInfoDTO.code}
                                                        </c:when>
                                                        <c:otherwise>
                                                            NA
                                                        </c:otherwise>
                                                    </c:choose>

                                                </p>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>ClientType</label>
                                                <p class="well well-sm">${clientProfileInfo.clientTypeTitle}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>City</label>
                                                <p class="well well-sm">${clientProfileInfo.cityName}</p>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Spouse Name</label>
                                                <p class="well well-sm">${clientProfileInfo.spouseName}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Father Name</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.fatherName}
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Grand Father Name</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.grandFatherName}
                                                </p>
                                            </div>
                                        </div>

                                        <div class="row">

                                            <div class="col-md-4">
                                                <label>No. of Male</label>
                                                <p class="well well-sm">${clientProfileInfo.noOfMale}</p>
                                            </div>

                                            <div class="col-md-4">
                                                <label>No. of Female</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.noOfFemale}
                                                </p>
                                            </div>

                                            <div class="col-md-4">
                                                <label>Total Person</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.totalPerson}
                                                </p>
                                            </div>

                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">
                                                <label>Land Lord's Name</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.landLordName}
                                                </p>
                                            </div>
                                            <div class="col-md-6">
                                                <label>Land Lord's Address</label>
                                                <p class="well well-sm">${clientProfileInfo.landLordAddress}</p>
                                            </div>
                                        </div>
                                        <div class="row">
                                            <div class="col-md-3">
                                                <label>Land Kitta No.</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.landKittaNo}
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Land Naksa No.</label>
                                                <p class="well well-sm">
                                                        ${clientProfileInfo.landNaksaNo}
                                                </p>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Land Volume</label>
                                                <p class="well well-sm">${clientProfileInfo.landVolume}</p>
                                            </div>
                                            <div class="col-md-3">
                                                <label>Land Volume Unit</label>
                                                <p class="well well-sm">${clientProfileInfo.landVolumeUnit}</p>
                                            </div>
                                        </div>

                                        <div class="row">
                                            <div class="col-md-6">

                                                <p class="pull-left well well-sm">
                                                    <label class="pull-left">Client Id Card</label><br>
                                                    <c:choose>
                                                        <c:when test="${clientProfileInfo.idPhotoName ne null}">
                                                            <a href="${pageContext.request.contextPath}/image/${clientProfileInfo.idPhotoName}"
                                                               target="_blank"><img
                                                                    src="${pageContext.request.contextPath}/image/${clientProfileInfo.idPhotoName}"
                                                                    style="width:200px"></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg"
                                                                 style="width:200px">
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>

                                            <div class="col-md-6">

                                                <p class="pull-right  well well-sm">
                                                    <label class="pull-right">Lalpurza</label><br>
                                                    <c:choose>
                                                        <c:when test="${clientProfileInfo.lalpurzaphotoName ne null}">
                                                            <a href="${pageContext.request.contextPath}/image/${clientProfileInfo.lalpurzaphotoName}"
                                                               target="_blank"><img
                                                                    src="${pageContext.request.contextPath}/image/${clientProfileInfo.lalpurzaphotoName}"
                                                                    style="width:200px"></a>
                                                        </c:when>
                                                        <c:otherwise>
                                                            <img src="${pageContext.request.contextPath}/resources/img/dummy.jpg"
                                                                 style="width:200px">
                                                        </c:otherwise>
                                                    </c:choose>
                                                </p>
                                            </div>
                                        </div>
                                        <c:if test="${clientProfileInfo.profileStatus.value ne 'Approved'}">
                                            <div class="row margin well well-sm">
                                                <div class="col-md-12 ">

                                                        <sec:authorize access="hasRole('Role_ClientProfileInfo_Delete')">
                                                            <a href="${pageContext.request.contextPath}/clientapplication/delete/${clientProfileInfo.clientProfileInfoId}"
                                                               class="btn btn-danger btn-sm btn-flat pull-left confirmDelete"
                                                               data-type="you want to delete "><span
                                                                    class="glyphicon glyphicon-trash"></span>&nbsp;delete</a>
                                                        </sec:authorize>

                                                        <sec:authorize access="hasAnyRole('System' , 'Admin')">
                                                            <a href="${pageContext.request.contextPath}/clientapplication/approve/${clientProfileInfo.clientProfileInfoId}"
                                                               class="btn btn-success btn-sm btn-flat pull-right confirmDelete"
                                                               data-type="you want to Approve "><span
                                                                    class="glyphicon glyphicon-save"></span>&nbsp;approve</a>
                                                        </sec:authorize>
                                                </div>

                                                <%--<div class="col-md-6 pull-right">
                                                    <sec:authorize access="hasRole('Role_ClientProfileInfo_Update')">
                                                        <a href="${pageContext.request.contextPath}/clientapplication/edit/${clientProfileInfo.clientProfileInfoId}"
                                                        class="btn btn-warning btn-sm pull-right confirmDelete" data-type="application">
                                                            <span class="glyphicon glyphicon-pencil"></span>&nbsp;edit</a>
                                                    </sec:authorize>
                                                </div>--%>
                                            </div>
                                        </c:if>
                                    </c:when>
                                    <c:otherwise>
                                        <p class="form-error">Client Application Not Found</p>
                                    </c:otherwise>
                                </c:choose>

                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                </div>
            </section>

            <sec:authorize access="hasRole('Role_ClientProfileInfo_Update')">
            <section class="content">
                <div class="row">
                    <div class="col-md-12">

                        <div class="box box-success">
                            <div class="box-header with-border">
                                <h3 class="box-title">File Upload</h3>
                            </div>
                            <div class="box-body">


                                <div class="row">
                                        <div class="col-md-4">
                                            <form action="${pageContext.request.contextPath}/clientapplication/upload/document"
                                                  method="post" enctype="multipart/form-data">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="control-label">Choose Client Photo</label>
                                                        <input type="file" class="form-control" name="doc" placeholder="select client photo" required>
                                                        <input type="hidden" name="type" value="photo">
                                                        <p class="form-error">${clientProfileInfoError.photo}</p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                           value="${_csrf.token}"/>
                                                    <input type="hidden" name="clientProfileId"
                                                           value="${clientProfileInfo.clientProfileInfoId}"/>
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-upload"></i>&nbsp;Change Photo
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="col-md-4">
                                            <form action="${pageContext.request.contextPath}/clientapplication/upload/document"
                                                  method="post" enctype="multipart/form-data">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="control-label">
                                                            Choose Client Id (citizenship or any )
                                                        </label>
                                                        <input type="file" class="form-control" name="doc" required>
                                                        <input type="hidden" name="type" value="idPhoto">
                                                        <p class="form-error">${clientProfileInfoError.idPhoto}</p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                           value="${_csrf.token}"/>
                                                    <input type="hidden" name="clientProfileId"
                                                           value="${clientProfileInfo.clientProfileInfoId}"/>
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-upload"></i>&nbsp;Change Id
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                        <div class="col-md-4">
                                            <form action="${pageContext.request.contextPath}/clientapplication/upload/document"
                                                  method="post" enctype="multipart/form-data">
                                                <div class="box-body">
                                                    <div class="form-group">
                                                        <label class="control-label">Choose Lalpurza</label>
                                                        <input type="file" class="form-control" name="doc" required>
                                                        <input type="hidden" name="type" value="lalpurzaPhoto">
                                                        <p class="form-error">${clientProfileInfoError.lalpurzaPhoto}</p>
                                                    </div>
                                                </div>
                                                <div class="modal-footer">
                                                    <input type="hidden" name="${_csrf.parameterName}"
                                                           value="${_csrf.token}"/>
                                                    <input type="hidden" name="clientProfileId"
                                                           value="${clientProfileInfo.clientProfileInfoId}"/>
                                                    <button type="submit" class="btn btn-primary">
                                                        <i class="fa fa-upload"></i>&nbsp; Change Lalpurza
                                                    </button>
                                                </div>
                                            </form>
                                        </div>

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>
            </sec:authorize>

        </div>
    </jsp:body>

</t:genericpage>

<script>
    $(document).ready(function () {
    	
    	
    	
    	function collectDate(callback){
    		var data = $('.basePrint').html();
    		callback(data);
    	}
    	
    	function printData(data){
    		 var mywindow = window.open('', 'my div', 'height=400,width=600');
             mywindow.document.write('<html><head><style></style><title></title>');
             mywindow.document.write('<link rel="stylesheet" href="file:///C:/Users/rosha/OneDrive/Desktop/style.css" type="text/css" />');

             var myvar11 = '* {' +
                 '	box-sizing: border-box;' +
                 '}' +
                 '' +
                 '.wrapper {' +
                 '	display: flex;' +
                 '	justify-content: center;' +
                 '	align-items: center;' +
                 '	font-family: Montserrat;' +
                 '	background: #262626;' +
                 '	width: 100%;' +
                 '	height: 100vh;' +
                 '}' +
                 '' +
                 '.outer {' +
                 '	position: relative;' +
                 '	background: #fff;' +
                 '	height: 350px;' +
                 '	width: 550px;' +
                 '	overflow: hidden;' +
                 '	display: flex;' +
                 '	align-items: center;' +
                 '}' +
                 '' +
                 'img {' +
                 '	position: absolute;' +
                 '	top: 0px;' +
                 '	right: -20px;' +
                 '	z-index: 0;' +
                 '	animation-delay: 0.5s;' +
                 '}' +
                 '' +
                 '.content {' +
                 '	animation-delay: 0.3s;' +
                 '	position: absolute;' +
                 '	left: 20px;' +
                 '	z-index: 3' +
                 '	' +
                 '}' +
                 '' +
                 'h1 {' +
                 '	color: #111;' +
                 '}' +
                 '' +
                 'p {' +
                 '	width: 280px;' +
                 '	font-size: 13px;' +
                 '	line-height: 1.4;' +
                 '	color: #aaa;' +
                 '	margin: 20px 0;' +
                 '	' +
                 '}' +
                 '' +
                 '.bg {' +
                 '	display: inline-block;' +
                 '	color: #fff;' +
                 '	background: cornflowerblue;' +
                 '	padding: 5px 10px;' +
                 '	border-radius: 50px;' +
                 '	font-size: .7em;' +
                 '}' +
                 '.button {' +
                 '	width: fit-content;' +
                 '	height: fit-content;' +
                 '	margin-top: 10px;' +
                 '	' +
                 '	' +
                 '	' +
                 '}' +
                 '' +
                 '.button a {' +
                 '	display: inline-block;' +
                 '	overflow: hidden;' +
                 '	position: relative;' +
                 '	font-size: 11px;' +
                 '	color: #111;' +
                 '	text-decoration: none;' +
                 '	padding: 10px 15px;' +
                 '	border: 1px solid #aaa;' +
                 '	font-weight: bold;' +
                 '	' +
                 '	' +
                 '}' +
                 '' +
                 '.button a:after{' +
                 '	content: "";' +
                 '	position: absolute;' +
                 '	top: 0;' +
                 '	right: -10px;' +
                 '	width: 0%;' +
                 '	background: #111;' +
                 '	height: 100%;' +
                 '	z-index: -1;' +
                 '	transition: width 0.3s ease-in-out;' +
                 '	transform: skew(-25deg);' +
                 '	transform-origin: right;' +
                 '}' +
                 '' +
                 '.button a:hover:after {' +
                 '	width: 150%;' +
                 '	left: -10px;' +
                 '	transform-origin: left;' +
                 '	' +
                 '}' +
                 '' +
                 '.button a:hover {' +
                 '	color: #fff;' +
                 '	transition: all 0.5s ease;' +
                 '}' +
                 '' +
                 '' +
                 '.button a:nth-of-type(1) {' +
                 '	border-radius: 50px 0 0 50px;' +
                 '	border-right: none;' +
                 '}' +
                 '' +
                 '.button a:nth-of-type(2) {' +
                 '	border-radius: 0px 50px 50px 0;' +
                 '}' +
                 '' +
                 '.cart-icon {' +
                 '	padding-right: 8px;' +
                 '	' +
                 '}' +
                 '' +
                 '.footer {' +
                 '	position: absolute;' +
                 '	bottom: 0;' +
                 '	right: 0;' +
                 '}';


             mywindow.document.write('<style type="text/css">' + myvar11 + ' </style></head><body>');
             mywindow.document.write(data);
             mywindow.document.write('</body></html>');
             mywindow.document.close();
             mywindow.print();
    	}
    	
    	
    	

        $('.printclick').click(function () {
           
        	collectDate(printData);

        });

        $(function () {
            $.fn.dataTable.ext.errMode = 'none';

            $('#ajaxDataTable').on('error.dt', function (e, settings, techNote, message) {
                console.log('An error has been reported by DataTables: ', message);
            }).DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                ordering: true,
                lengthChange: true,
                ajax: $('#ajaxDataTable').attr('url'),
                columnDefs: [{
                    targets: 6,
                    render: function (data, type, row) {

                        var result = "";
                        if (row[6] === "Na") {
                            result = '<a href="${pageContext.request.contextPath}/clientunit/invoice/add/' + row[0] + '" ><span class="label label-info">Generate Bill</span></a>';
                        } else if (row[6] === "Unpaid") {
                            result = '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" ><span class="label label-danger">Unpaid</span></a>';
                        } else if (row[6] === "Partial") {
                            result = '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" ><span class="label label-warning">Partial</span></a>';
                        } else if (row[6] === "Full") {
                            result = '<a href="${pageContext.request.contextPath}/clientunit/invoice/show/' + row[0] + '" ><span class="label label-success">Fullpaid</span></a>';
                        }
                        return result;
                    }
                }]
            });
        });

    });
</script>